package es.almata.persistencia;

public class Configuracio {
	public static final String PROTOCOL="jdbc:mysql://";
	public static final String IP_SERVIDOR="127.0.0.1";
	public static final String PORT_BBDD="3306";
	public static final String BBDD="practicaUF6_BG"; //practicaUF6_BG - uf6
	public static final String NO_USE_SSL="?useSSL=false";
	public static final String USUARI="root";
	public static final String PASSWORD="root";
	public static final String DRIVER_NAME="com.mysql.cj.jdbc.Driver";
}
