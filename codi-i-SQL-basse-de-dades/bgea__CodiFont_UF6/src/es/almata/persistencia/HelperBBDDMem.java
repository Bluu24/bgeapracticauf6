package es.almata.persistencia;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;


import es.almata.domini.Destinacio;
import es.almata.domini.Punt_Interes;










public class HelperBBDDMem {
	//SIMULACIO DE TAULA USUARIS AMB UN MAP
	
		//private static Map<String, Destinacio> taulaDestins= new TreeMap<String, Destinacio>();

	public static void addDesti(Destinacio desti) {
		GestorDB.modificaDB("INSERT INTO `Destinacions`(`provincia`, `comarca`, `ciutat_poblacio`, `cp`, `platja`, `muntanya`) VALUES ('"+desti.getProvincia()+"','"+desti.getComarca()+"','"+desti.getCiutat_poblacio()+"',"+desti.getCp()+","+desti.getPlatja()+","+desti.getMuntanya()+") ");
		System.out.println(desti);
	}
	
	
	public static Map<String,Destinacio> getAllDestins(){
		List<Destinacio> destins=GestorDB.consultaDB("SELECT * FROM Destinacions", Destinacio.class);	
		Map<String,Destinacio> mapaDestins = new TreeMap<String,Destinacio>();
		for(Destinacio desti: destins) {
			mapaDestins.put(desti.getCiutat_poblacio(), desti);
		}
		return mapaDestins;
	}
	
	
	public static Destinacio getDesti(String Key) {
		List<Destinacio> destins=GestorDB.consultaDB("SELECT * FROM Destinacions WHERE ciutat_poblacio='"+ Key +"'",Destinacio.class);	
		return  destins.get(0);

	}
	
	
	public static void modificarDesti(Destinacio desti) {
		GestorDB.modificaDB("UPDATE `Destinacions` SET `provincia`='"+desti.getProvincia()+"',`comarca`='"+desti.getComarca()+"',`ciutat_poblacio`='"+desti.getCiutat_poblacio()+"',`cp`="+desti.getCp()+",`platja`="+desti.getPlatja()+",`muntanya`="+desti.getMuntanya()+" WHERE ciutat_poblacio='"+desti.getCiutat_poblacio()+"'");

	}
	
	public static void removeDesti(String nomCiutat) {
		GestorDB.modificaDB("DELETE FROM Destinacions WHERE ciutat_poblacio='"+nomCiutat+"'");
		
		
	}
	
	//--------------------------------------------
	public static void addPunInt(Punt_Interes puntInt){
		GestorDB.modificaDB("INSERT INTO `Punts_Interes`(`id`, `nom`, `descripcio`, `imatge`, `platja`, `muntanya`, `gastronomic`, `cultural`, `oci`, `esport`, `rural`, `esqui`, `natacio`, `escalada`, `compres`, `equitacio`, `senderisme`, `fk_Destinacio`) VALUES ("+puntInt.getID()+",'"+puntInt.getNom()+"','"+puntInt.getDescripcio()+"','"+puntInt.getImatge()+"',"+puntInt.getPlatja()+","+puntInt.getMuntanya()+","+puntInt.getGastronomic()+","+puntInt.getCultural()+","+puntInt.getOci()+","+puntInt.getEsport()+","+puntInt.getRural()+","+puntInt.getEsqui()+","+puntInt.getNatacio()+","+puntInt.getEscalada()+","+puntInt.getCompres()+","+puntInt.getEquitacio()+","+puntInt.getSenderisme()+",'"+puntInt.getfk_Destinacio()+"') ");
	
		System.out.println(puntInt);
		
	}
	
	public static Map<String,Punt_Interes> getAllPuntI(String desti){
		List<Punt_Interes> puntInts= GestorDB.consultaDB("SELECT * from Punts_Interes WHERE fk_Destinacio='"+desti+"'", Punt_Interes.class);
		Map<String,Punt_Interes> mapaPuntInt = new TreeMap<String,Punt_Interes>();
		if(puntInts!=null) {
			for(Punt_Interes puntInt: puntInts) {
				mapaPuntInt.put(puntInt.getNom(), puntInt);
			}
		}
		
		return mapaPuntInt;
	}
	
	public static Map<String,Punt_Interes> getAllPuntI2(){
		List<Punt_Interes> puntInts= GestorDB.consultaDB("SELECT * from Punts_Interes", Punt_Interes.class);
		Map<String,Punt_Interes> mapaPuntInt = new TreeMap<String,Punt_Interes>();
		if(puntInts!=null) {
			for(Punt_Interes puntInt: puntInts) {
				mapaPuntInt.put(puntInt.getNom(), puntInt);
			}
		}
		
		return mapaPuntInt;
	}
	
	public static Punt_Interes getPuntInt(String key) {
		List<Punt_Interes> punts_int=GestorDB.consultaDB("SELECT * FROM Punts_Interes WHERE nom='"+ key +"'",Punt_Interes.class);	
		return  punts_int.get(0);

	}
	
	public static void removePuntInt(int id) {
		GestorDB.modificaDB("DELETE FROM Punts_Interes WHERE id='"+id+"'");
		
		
	}
	
	public static void modificarPuntInt(Punt_Interes punt) {
		GestorDB.modificaDB("UPDATE `Punts_Interes` SET `id`="+punt.getID()+",`nom`='"+punt.getNom()+"',`descripcio`='"+punt.getDescripcio()+"',`imatge`='"+punt.getImatge()+"',`platja`="+punt.getPlatja()+",`muntanya`="+punt.getMuntanya()+",`gastronomic`="+punt.getGastronomic()+",`cultural`="+punt.getCultural()+",`oci`="+punt.getOci()+",`esport`="+punt.getEsport()+",`rural`="+punt.getRural()+",`esqui`="+punt.getEsqui()+",`natacio`="+punt.getNatacio()+",`escalada`="+punt.getEscalada()+",`compres`='"+punt.getCompres()+",`equitacio`="+punt.getEquitacio()+",`senderisme`="+punt.getSenderisme()+",`fk_Destinacio`='"+punt.getfk_Destinacio()+"' WHERE id="+punt.getID()+"");
		
		
		
	}
	
	
	
	
	
	
	
	



}

