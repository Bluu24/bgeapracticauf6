package es.almata.precentacio;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.text.MaskFormatter;

import es.almata.domini.Destinacio;
import es.almata.utils.Encriptar;
import es.almata.utils.Fonts;
import es.almata.utils.Util;

public class FormulariModifiDesti extends JInternalFrame implements Formulari{

	private static final String COMARCA = "Comarca:";
	private static final String PROBINCIÁ = "Probinciá: ";
	private static final String TÉ_PLATJA_O_MUNTANYA = "Té Platja o Muntanya: ";
	private static final String MUNTANYA = "Muntanya";
	private static final String PLATJA = "Platja";
	private static final String BOTO_CANCELAR = "Cancelar";
	private static final String BOTO_MODIFI = "Modoficar";
	private static final String CODI_POSTAL_CP = "Codi Postal (CP): ";
	private static final String CIUTAT_POBLACIÓ = "Ciutat / Població: ";
	
	
	private static final long serialVersionUID = 1L;
	private GridBagLayout layout;
	private Controlador controlador;
	private FormulariModifiDesti fdm;
	
	//---------------------------
	
	private JLabel lblCiutatPoblacio;
	private JTextField txtCiutatPoblacio;
	//---------------------------
	
	private JLabel lblCodiPostal;
	private JTextField txtCodiPostal;
	//---------------------------
	
	private JButton btnModificar;
	private JButton btnCancelar;
	//---------------------------
	
	private JPanel pnlBotons;
	private JRadioButton rbtPlatja;
	private JRadioButton rbtMuntanya;
	private JLabel lblPlantjaMuntanya;
	//---------------------------
	
	private ButtonGroup grupBotonsPM;
	private JLabel lblProvincia;
	private JTextField txtProvincia;
	//---------------------------
	
	private JLabel lblComarca;
	private JTextField txtComarca;
	private JComboBox<String> cmbDesti;
	
	public FormulariModifiDesti() {
		inicialitzacions();
		crearComponents();
		afegirComponenets();
		posicionarComponents();
		setVisible(true);
	}
	
	@Override
	public void inicialitzacions() {
		Container c=this.getContentPane();
		((JComponent) c).setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GREEN),"Modificacio de Destinació",TitledBorder.CENTER,TitledBorder.DEFAULT_JUSTIFICATION,Fonts.fontTitol(),Color.GREEN));
		
		getContentPane().setLayout(layout= new GridBagLayout());
		controlador= new Controlador();
		fdm=this;
		
		Util.treureBarraTitolInteralFrame(this);
		
	}


	@Override
	public void crearComponents() {
		lblCiutatPoblacio = new JLabel(CIUTAT_POBLACIÓ);
		txtCiutatPoblacio = new JTextField();
		txtCiutatPoblacio.setFont(Fonts.fontTextField());
		txtCiutatPoblacio.setEnabled(false);
		//---------------------------
		lblCodiPostal = new JLabel(CODI_POSTAL_CP);
		txtCodiPostal = new JTextField();
		txtCodiPostal.setFont(Fonts.fontTextField());
		
		MaskFormatter mascara = null;	
		try {
			mascara= new MaskFormatter("#####");
			mascara.setPlaceholderCharacter('_');
		} catch (ParseException e) {
			e.printStackTrace();
		}		
		txtCodiPostal= new JFormattedTextField(mascara);
		txtCodiPostal.setEnabled(false);
		//---------------------------
		btnModificar = new JButton(BOTO_MODIFI);
		btnModificar.addActionListener(controlador);
		btnModificar.setActionCommand(BOTO_MODIFI);
		btnCancelar = new JButton(BOTO_CANCELAR);
		btnCancelar.addActionListener(controlador);
		btnCancelar.setActionCommand(BOTO_CANCELAR);
		pnlBotons= new JPanel();
		pnlBotons.add(btnModificar);
		pnlBotons.add(btnCancelar);
		//---------------------------
		lblPlantjaMuntanya = new JLabel(TÉ_PLATJA_O_MUNTANYA);
		rbtPlatja= new JRadioButton(PLATJA);
		rbtMuntanya= new JRadioButton(MUNTANYA);
		grupBotonsPM= new ButtonGroup();
		grupBotonsPM.add(rbtPlatja);
		grupBotonsPM.add(rbtMuntanya);
		rbtPlatja.setEnabled(false);
		rbtMuntanya.setEnabled(false);
		//---------------------------
		lblProvincia = new JLabel(PROBINCIÁ);
		txtProvincia = new JTextField();
		txtProvincia.setFont(Fonts.fontTextField());
		txtProvincia.setEnabled(false);
		//---------------------------
		lblComarca = new JLabel(COMARCA);
		txtComarca = new JTextField();
		txtComarca.setFont(Fonts.fontTextField());
		txtComarca.setEnabled(false);
		//---------------------------
		cmbDesti= new JComboBox<String>(omplirCombo());
		cmbDesti.setSelectedIndex(0); //per defecte esta seleccionat la primera opcio -->"tria usuari"
		cmbDesti.setActionCommand("combo");
		cmbDesti.addActionListener(controlador);
		
	}
	
	private Vector<String> omplirCombo(){
		Map<String,Destinacio> desti= ControladorPre.getAllDestins();
		Vector<String> dadesCombo= new Vector<String>();
		dadesCombo.add("TRIA DESTINACIO ");
		for(Entry<String,Destinacio> key: desti.entrySet()) {
			dadesCombo.add(key.getValue().getCiutat_poblacio());
		}
		return dadesCombo;
	}

	@Override
	public void afegirComponenets() {
		
		this.getContentPane().add(lblCiutatPoblacio);
		this.getContentPane().add(txtCiutatPoblacio);
		this.getContentPane().add(lblCodiPostal);
		this.getContentPane().add(txtCodiPostal);
		this.getContentPane().add(pnlBotons);
		this.getContentPane().add(lblPlantjaMuntanya);
		this.getContentPane().add(rbtPlatja);
		this.getContentPane().add(rbtMuntanya);
		this.getContentPane().add(txtProvincia);
		this.getContentPane().add(lblProvincia);
		this.getContentPane().add(lblComarca);
		this.getContentPane().add(txtComarca);
		this.getContentPane().add(cmbDesti);
	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc= new GridBagConstraints();
		
		//RESTRINCCIONS DEL COMBO
		gbc.gridx=0;//columna
		gbc.gridy=0;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(cmbDesti,gbc);
		
		
		
		
		//RESTRINCCIONS DE LA ETIQUETA (Provincies)...
		gbc.gridx=0;//columna
		gbc.gridy=1;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.BOTH;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(lblProvincia,gbc);
		
		
		//RESTRINCCIONS  DEL QUADRO DE TEXT PROVINCIA
		gbc.gridx=1;//columna
		gbc.gridy=1;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.HORIZONTAL;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(txtProvincia,gbc);
		
		//RESTRINCCIONS DE LA ETIQUETA (Comarca)...
		gbc.gridx=0;//columna
		gbc.gridy=2;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.BOTH;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(lblComarca,gbc);
		
		//RESTRINCCIONS DEL QUADRO DE TEXT COMARCA
		gbc.gridx=1;//columna
		gbc.gridy=2;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.HORIZONTAL;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(txtComarca,gbc);
		
		//RESTRINCCIONS DE LA ETIQUETA (Ciutat / Poblacio)...
		gbc.gridx=0;//columna
		gbc.gridy=3;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.BOTH;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(lblCiutatPoblacio,gbc);
		
		//RESTRINCCIONS DEL QUADRO DE TEXT Ciutat / Poblacio
		gbc.gridx=1;//columna
		gbc.gridy=3;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.HORIZONTAL;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(txtCiutatPoblacio,gbc);
		
		//RESTRINCCIONS DE LA ETIQUETA (Codi Postal)...
		gbc.gridx=0;//columna
		gbc.gridy=4;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.BOTH;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(lblCodiPostal,gbc);
				
		//RESTRINCCIONS DEL QUADRO DE TEXT Codi Postal
		gbc.gridx=1;//columna
		gbc.gridy=4;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(txtCodiPostal,gbc);
		
		//RESTRINCCIONS DEL LA ETIQUETA DE TRIA EL PLATJA O MUNTANYA
		gbc.gridx=0;//columna
		gbc.gridy=5;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		//gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(lblPlantjaMuntanya,gbc);
				
		//RESTRINCCIONS DEL BOTO PLATJA
		gbc.gridx=1;//columna
		gbc.gridy=5;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(rbtPlatja,gbc);
				
		//RESTRINCCIONS DEL BOTO MUNTANYA
		gbc.gridx=1;//columna
		gbc.gridy=6;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(rbtMuntanya,gbc);
		

		//RESTRINCCIONS DEL PANEL
		gbc.gridx=1;//columna
		gbc.gridy=8;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.EAST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(pnlBotons,gbc);
		
		
	}
	
	public class Controlador implements ActionListener{
		String cami;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj=e.getSource();
			
			if(obj instanceof JButton) {
				JButton boto= (JButton) obj;
				if(boto.getActionCommand().equals(BOTO_MODIFI)) {
					System.out.println("He clicat el botó acceptar!!");	
					if(dadesCorrectes()) {
						int tempint;
						try {
							tempint=Integer.parseInt(txtCodiPostal.getText());
							ControladorPre.modificarDesti(getStringMessageDigest(txtProvincia.getText(), Encriptar.MD2) , txtComarca.getText(),txtCiutatPoblacio.getText() , tempint
									,rbtPlatja.isSelected(),rbtMuntanya.isSelected());
							ControladorPre.canviPantalla(new FormulariDefault());
						}catch(NumberFormatException nfe) {
							Object[]objTExtBotons= {"Acceptar"};
							
							JOptionPane.showOptionDialog(fdm, "El Codi Postal (CP) ha de ser un numero enter", "IMPORTANT", JOptionPane.OK_OPTION , JOptionPane.WARNING_MESSAGE, null, objTExtBotons, null);
						}	
						
					}
					
					
				}else if(boto.getActionCommand().equals(BOTO_CANCELAR)) {
					System.out.println("He clicat el botó cancel·lar!!");
					ControladorPre.canviPantalla(new FormulariDefault());
				}
			}else if(obj instanceof JComboBox<?>) {
				if(cmbDesti.getActionCommand().equals("combo")) {
					omplirFormulariDestiSeleccionat();
					txtProvincia.setEnabled(true);
					txtComarca.setEnabled(true);
					txtCodiPostal.setEnabled(true);
					rbtPlatja.setEnabled(true);
					rbtMuntanya.setEnabled(true);
				}
			}			
		}
		
		private void omplirFormulariDestiSeleccionat() {
			if(cmbDesti.getSelectedIndex()!=0) {
				String nomCiutat= String.valueOf( cmbDesti.getSelectedItem());
				System.out.println("Nom del Desti selecionat combo: "+ nomCiutat);
				afegirDestiFormulari(nomCiutat);	
			}
			
		}
		
	
		private void afegirDestiFormulari(String nomCiutat) {
			Destinacio desti= ControladorPre.getDesti(nomCiutat);
		
			txtProvincia.setText(desti.getProvincia());
			txtComarca.setText(desti.getComarca());
			txtCiutatPoblacio.setText(desti.getCiutat_poblacio());
			txtCodiPostal.setText(String.valueOf(desti.getCp()));
			rbtPlatja.setSelected(desti.getPlatja()); 
			rbtMuntanya.setSelected(desti.getMuntanya()); 
		}
		
		


		private boolean dadesCorrectes() {
			boolean correcte= true;
			if(txtCiutatPoblacio.getText().isEmpty()) {
				System.out.println("FALTE EL NOM !!");
				correcte= false;
			}
			return correcte;
		}
		
		private static String toHexadecimal(byte[] digest){
	        String hash = "";
	        for(byte aux : digest) {
	            int b = aux & 0xff;
	            if (Integer.toHexString(b).length() == 1) hash += "0";
	            hash += Integer.toHexString(b);
	        }
	        return hash;
	    }
		
		 public static String getStringMessageDigest(String message, String algorithm){
		        byte[] digest = null;
		        byte[] buffer = message.getBytes();
		        try {
		            MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
		            messageDigest.reset();
		            messageDigest.update(buffer);
		            digest = messageDigest.digest();
		        } catch (NoSuchAlgorithmException ex) {
		            System.out.println("Error creant el Digest...");
		        }
		        return toHexadecimal(digest);
		    } 
		
	}

}
