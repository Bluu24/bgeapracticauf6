package es.almata.precentacio;

public interface Formulari {
	public abstract void inicialitzacions();
	public abstract void crearComponents();
	public abstract void afegirComponenets();
	public abstract void posicionarComponents();

}
