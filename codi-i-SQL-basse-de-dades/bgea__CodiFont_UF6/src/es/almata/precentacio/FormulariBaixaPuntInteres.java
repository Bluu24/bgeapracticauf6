package es.almata.precentacio;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;
import java.util.Map.Entry;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import es.almata.precentacio.ControladorPre;
import es.almata.precentacio.FormulariDefault;
import es.almata.domini.Destinacio;
import es.almata.domini.Punt_Interes;
import es.almata.utils.Fonts;
import es.almata.utils.Util;

public class FormulariBaixaPuntInteres extends JInternalFrame implements Formulari {
	

	private static final Object BOTO_BAIXA = "baixa";
	private static final String TRIA_EL_DESTINACIO = "TRIA DESTINACIO  ";

	private static final long serialVersionUID = 1L;

	private GridBagLayout layout;
	private Controlador controlador;
	private FormulariBaixaPuntInteres fpb;
	
	//---------------------------

	private JComboBox<String> cmbDestinacions;
	private DefaultTableModel model;
	private JTable taula;
	private JLabel lbltaula;
	private JButton btnCancelar;
	private JScrollPane slpTaula;
	private JButton btnBaixa;
	private JPanel panell;
		
	//---------------------------
	
	public FormulariBaixaPuntInteres() {
		inicialitzacions();
		crearComponents();
		afegirComponenets();
		posicionarComponents();
		setVisible(true);
	}
	
	@Override
	public void inicialitzacions() {
		Container c=this.getContentPane();
		((JComponent) c).setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY),"Baixa del Punt d'Interes",TitledBorder.CENTER,TitledBorder.DEFAULT_JUSTIFICATION,Fonts.fontTitol(),Color.GRAY));
		
		getContentPane().setLayout(layout= new GridBagLayout());
		controlador= new Controlador();
		fpb=this;
		
		Util.treureBarraTitolInteralFrame(this);
	}

	//---------------------------

	@Override
	public void crearComponents() {
		//definicio jcombo
		
		cmbDestinacions= new JComboBox<String>(omplirCombo());
		cmbDestinacions.setSelectedIndex(0);
		cmbDestinacions.setActionCommand("combo");
		cmbDestinacions.addActionListener(controlador);
		
		
		//definicio taula . Classe anonima
		model= new DefaultTableModel() {
			private static final long serialVersionUID = 1L;
			@Override
			public boolean isCellEditable( int row, int colum) {
				return false;
			}
		};
		
		//Afeguir columnes a la taula
		model.addColumn("ID");
		model.addColumn("Nom");
		model.addColumn("Descripcio");
		model.addColumn("Gastronòmic");
		model.addColumn("Cultural");
		model.addColumn("Oci");
		model.addColumn("Esport");
		model.addColumn("Platja");
		model.addColumn("Muntanya");
		model.addColumn("Rural");
		model.addColumn("Esquí");
		model.addColumn("Natació");
		model.addColumn("Escalada");
		model.addColumn("Compres");
		model.addColumn("Equitació");
		model.addColumn("Senderisme");
		model.addColumn("Imatge");
		
		taula= new JTable(model);
		taula.addMouseListener(controlador);
		taula.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		
		slpTaula= new JScrollPane(taula);
		slpTaula.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		slpTaula.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		
		
		
		//label
		lbltaula= new JLabel("Punt d'Interes: ");
		
		
		
		btnCancelar= new JButton("Cancelar");
		btnCancelar.addActionListener(controlador);
		btnCancelar.setActionCommand("cancelar");
		
		btnBaixa= new JButton("Baixa");
		btnBaixa.addActionListener(controlador);
		btnBaixa.setActionCommand("baixa");
		
		panell= new JPanel(new GridLayout());
		panell.add(btnBaixa);
		panell.add(btnCancelar);
		
		
	}
	
	private Vector<String> omplirCombo(){
		Map<String,Destinacio> desti= ControladorPre.getAllDestins();
		
		Vector<String> dadesCombo= new Vector<String>();
		dadesCombo.add(TRIA_EL_DESTINACIO);
		for(Entry<String, Destinacio> key: desti.entrySet()) {
			dadesCombo.add(key.getValue().getCiutat_poblacio());
		}
		return dadesCombo;
	}
	
	
	//---------------------------


	@Override
	public void afegirComponenets() {
		add(cmbDestinacions);
		add(slpTaula);
		add(lbltaula);
		add(panell);
		
	}
	//---------------------------


	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc= new GridBagConstraints();
		
		//RESTRINCCIONS DEL COMBO
		gbc.gridx=0;//columna
		gbc.gridy=0;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(cmbDestinacions,gbc);
		
		
		//RESTRINCCIONS DE LA ETIQUETA TAULA
		gbc.gridx=0;//columna
		gbc.gridy=2;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes	
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(lbltaula,gbc);
		
		//RESTRINCCIONS DE LA TAULA
		gbc.gridx=0;//columna
		gbc.gridy=3;//fila
		gbc.gridheight=2;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 1;//creixement columna
		gbc.weighty= 1;//creixement fila
		//gbc.anchor=GridBagConstraints.NONE;
		gbc.fill=GridBagConstraints.BOTH;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(slpTaula,gbc);
		
		//RESTRINCCIONS DEL panell
		gbc.gridx=0;//columna
		gbc.gridy=5;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.EAST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(panell,gbc);
		
		 
		
	}

	//---------------------------

	public class Controlador extends MouseAdapter implements ActionListener{
	String nom;
	
	
	int fila;
	int columna;
		
		@Override
		public void actionPerformed(ActionEvent e) {
		Object obj=e.getSource();
			
			if(obj instanceof JButton) {
				JButton boto= (JButton) obj;
				if(boto.getActionCommand().equals(BOTO_BAIXA)) {
					
					Object[]objTExtBotons= {"Acceptar", "Cancel·lar"};
					
					int n= JOptionPane.showOptionDialog(fpb, "Segur que vols fer la baixa?", "IMPORTANT", JOptionPane.OK_CANCEL_OPTION , JOptionPane.WARNING_MESSAGE, null, objTExtBotons, objTExtBotons[1]);
					//ControladorPre.removePuntInt((Integer)model.getValueAt(fila, columna));
					//model.removeRow(fila);//esbora la fila de la taula graficament 
					
					if(!String.valueOf( cmbDestinacions.getSelectedItem()).equals(TRIA_EL_DESTINACIO)&& n==0) {
						
						ControladorPre.removePuntInt((Integer)model.getValueAt(fila, columna));
						model.removeRow(fila);//esbora la fila de la taula graficament 
					}
					
				}
				ControladorPre.canviPantalla(new FormulariDefault());
			}else if(obj instanceof JComboBox<?>) { 
				if(cmbDestinacions.getActionCommand().equals("combo")) {
					omplirTaulaPuntInteresSeleccionat();
					
				}
			}
			
		}
		
	
	

		private void omplirTaulaPuntInteresSeleccionat() {
			model.setRowCount(0);//borrar dades de la taula
			if(cmbDestinacions.getSelectedIndex()!=0) {
				
				Object[]objTExtBotons= {"Acceptar"};
				
				JOptionPane.showOptionDialog(fpb, "Seleciona la columna de la ID del punt dinteres que vols donar de baixa i apreta el boto BAIXA", "IMPORTANT", JOptionPane.OK_OPTION , JOptionPane.INFORMATION_MESSAGE, null, objTExtBotons, null);
				String nom= String.valueOf(cmbDestinacions.getSelectedItem());
				System.out.println("Nom del Desti selecionat al combo: "+ nom);
				afegirPuntInteresATaula(nom);	
				
			}
		}
		
		private void afegirPuntInteresATaula(String nomCiutat) {
			//Destinacio desti= ControladorPre.getDesti(nomCiutat);
			//System.out.println("Desti: " +desti + "Punt d'Interes: " +desti.getPuntIntS());
			
			Map<String, Punt_Interes> puntInt= new TreeMap <String, Punt_Interes>();
			puntInt= ControladorPre.getAllPuntI(nomCiutat);
			Object[] objFila= new Object[17];
			for(Entry<String,Punt_Interes> keyPunt: puntInt.entrySet()) {
				Punt_Interes punt= keyPunt.getValue();
				objFila[0]= punt.getID();
				objFila[1]= punt.getNom();
				objFila[2]= punt.getDescripcio();
				objFila[3]= punt.getGastronomic();
				objFila[4]= punt.getCultural();
				objFila[5]= punt.getOci();
				objFila[6]= punt.getEsport();
				objFila[7]= punt.getPlatja();
				objFila[8]= punt.getMuntanya();
				objFila[9]= punt.getRural();
				objFila[10]= punt.getEsqui();
				objFila[11]= punt.getNatacio();
				objFila[12]= punt.getEscalada();
				objFila[13]= punt.getCompres();
				objFila[14]= punt.getEquitacio();
				objFila[15]= punt.getSenderisme();
				objFila[16]= punt.getImatge();
				model.addRow(objFila);
				
				
			}
			
			
		}
			
		
		@Override
		public void mouseClicked(MouseEvent e) {
			
			fila= taula.rowAtPoint(e.getPoint()); //la fila de la taula
			columna= taula.columnAtPoint(e.getPoint()); //la columna
			
			if((fila>-1) && (columna>-1)) {
				System.out.println(model.getValueAt(fila, columna));
				System.out.println("DONA DE BAIXA LA ID:"+model.getValueAt(fila, 0));
			}
			
			
			
		}
			
		}
	}	



