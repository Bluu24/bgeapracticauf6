package es.almata.precentacio;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;

import es.almata.utils.Util;

public class FormulariDefault extends JInternalFrame implements Formulari {

	
	private static final long serialVersionUID = 1L;
	private GridBagLayout layout;
	private Controlador controlador;
	
	private JLabel logo;

	public FormulariDefault() {
		inicialitzacions();
		crearComponents();
		afegirComponenets();
		posicionarComponents();
		setVisible(true);
		
	}
	

	@Override
	public void inicialitzacions() {
		this.getContentPane().setLayout(layout= new GridBagLayout());
		this.setBorder(null);
		Util.treureBarraTitolInteralFrame(this);
		controlador= new Controlador();
		
		
		
	}

	@Override
	public void crearComponents() {
		logo= new JLabel(Util.redimensionarImatge("imatges/icono2.jpg", Aplicacio.AMPLADA, Aplicacio.ALCADA));
	}

	@Override
	public void afegirComponenets() {
		add(logo);
	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc= new GridBagConstraints();
		
		//RESTRINCCIONS DE LA TAULA
		gbc.gridx=0;//columna
		gbc.gridy=0;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.fill=GridBagConstraints.BOTH;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(logo ,gbc);
		
		
	}
	
	
	
	public class Controlador implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			
		}
		
		
	}
	
	

}
