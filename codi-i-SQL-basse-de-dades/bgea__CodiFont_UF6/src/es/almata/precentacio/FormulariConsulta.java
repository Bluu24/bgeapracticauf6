package es.almata.precentacio;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import es.almata.precentacio.ControladorPre;
import es.almata.domini.Destinacio;
import es.almata.domini.Punt_Interes;
import es.almata.utils.Fonts;
import es.almata.utils.Util;

public class FormulariConsulta extends JInternalFrame implements Formulari{

	private static final String ACCEPTAR = "Acceptar";

	private static final String TRIA_EL_DESTI = "TRIA DESTI ";
	
	private static final long serialVersionUID = 1L;

	private GridBagLayout layout;
	private Controlador controlador;
	private FormulariConsulta fc;
	//---------------------------
	private JComboBox<String> cmbDesti;
	private DefaultTableModel model;
	private JTable taula;
	private JLabel lbltaula;
	private JButton btnAceptar;
	private JScrollPane slpTaula;
	
	//---------------------------
	public FormulariConsulta() {
		inicialitzacions();
		crearComponents();
		afegirComponenets();
		posicionarComponents();
		setVisible(true);
	}
	
	@Override
	public void inicialitzacions() {
		Container c=this.getContentPane();
		((JComponent) c).setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.CYAN),"Dades Generals",TitledBorder.CENTER,TitledBorder.DEFAULT_JUSTIFICATION,Fonts.fontTitol(),Color.CYAN));
		
		getContentPane().setLayout(layout= new GridBagLayout());
		controlador= new Controlador();
		fc=this;
		
		Util.treureBarraTitolInteralFrame(this);
		
	}

	@Override
	public void crearComponents() {
		//definicio jcombo
		
		cmbDesti= new JComboBox<String>(omplirCombo());
		cmbDesti.setSelectedIndex(0); //per defecte esta seleccionat la primera opcio -->"tria usuari"
		cmbDesti.setActionCommand("combo");
		cmbDesti.addActionListener(controlador);
		
		
		//definicio taula . Classe anonima
		model= new DefaultTableModel() {
			private static final long serialVersionUID = 1L;
			@Override
			public boolean isCellEditable( int row, int colum) {
				return false;
			}
		};
		
		//Afeguir columnes a la taula
		model.addColumn("Nom");
		model.addColumn("Descripcio");
		model.addColumn("Gastronòmic");
		model.addColumn("Cultural");
		model.addColumn("Oci");
		model.addColumn("Esport");
		model.addColumn("Platja");
		model.addColumn("Muntanya");
		model.addColumn("Rural");
		model.addColumn("Esquí");
		model.addColumn("Natació");
		model.addColumn("Escalada");
		model.addColumn("Compres");
		model.addColumn("Equitació");
		model.addColumn("Senderisme");
		
		
		taula= new JTable(model);
		taula.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		
		slpTaula= new JScrollPane(taula);
		slpTaula.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		slpTaula.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		//label
		lbltaula= new JLabel("Punts d'Interes:");
		btnAceptar= new JButton(ACCEPTAR);
		btnAceptar.addActionListener(controlador);
		btnAceptar.setActionCommand(ACCEPTAR);
		
		
	}
	
	private Vector<String> omplirCombo(){
		Map<String,Destinacio> desti= ControladorPre.getAllDestins();
		Vector<String> dadesCombo= new Vector<String>();
		dadesCombo.add(TRIA_EL_DESTI);
		for(Entry<String,Destinacio> key: desti.entrySet()) {
			dadesCombo.add(key.getValue().getCiutat_poblacio());
		}
		return dadesCombo;
	}
	//---------------------------

	@Override
	public void afegirComponenets() {
		add(cmbDesti);
		add(slpTaula);
		add(lbltaula);
		add(btnAceptar);
		
	}

	@Override
	public void posicionarComponents() {
GridBagConstraints gbc= new GridBagConstraints();
		
		//RESTRINCCIONS DEL COMBO
		gbc.gridx=0;//columna
		gbc.gridy=0;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(cmbDesti,gbc);
		
		
		//RESTRINCCIONS DE LA ETIQUETA TAULA
		gbc.gridx=0;//columna
		gbc.gridy=1;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes	
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(lbltaula,gbc);
		
		//RESTRINCCIONS DE LA TAULA
		gbc.gridx=0;//columna
		gbc.gridy=2;//fila
		gbc.gridheight=2;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 1;//creixement columna
		gbc.weighty= 1;//creixement fila
		//gbc.anchor=GridBagConstraints.NONE;
		gbc.fill=GridBagConstraints.BOTH;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(slpTaula,gbc);
		
		//RESTRINCCIONS DE LA TAULA
		gbc.gridx=0;//columna
		gbc.gridy=4;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.EAST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(btnAceptar,gbc);
		
	}
	
	public class Controlador implements ActionListener{
	
		
		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj=e.getSource();
			
			if(obj instanceof JButton) {
				JButton boto= (JButton) obj;
				if(boto.getActionCommand().equals(ACCEPTAR)) {
					ControladorPre.canviPantalla(new FormulariDefault());
				}
			}else if(obj instanceof JComboBox<?>) {
				if(cmbDesti.getActionCommand().equals("combo")) {
					omplirTaulaPuntIntSeleccionat();
				}
			}
			
		}

		private void omplirTaulaPuntIntSeleccionat() {
			model.setRowCount(0);//borrar dades de la taula
			if(cmbDesti.getSelectedIndex()!=0) {
				String nomCiutat= String.valueOf( cmbDesti.getSelectedItem());
				System.out.println("Nom Desti selecionat combo: "+ nomCiutat);
				afegirPuntInteresATaula(nomCiutat);	
				
			}
		}

		private void afegirPuntInteresATaula(String nomCiutat) {
			//Destinacio desti= ControladorPre.getDesti(nomCiutat);
			//System.out.println("Desti: " +desti + "Punt d'Interes: " +desti.getPuntIntS());
			
			Map<String, Punt_Interes> puntInt= new TreeMap <String, Punt_Interes>();
			puntInt= ControladorPre.getAllPuntI(nomCiutat);
			Object[] objFila= new Object[15];
			for(Entry<String,Punt_Interes> keyPunt: puntInt.entrySet()) {
				Punt_Interes punt= keyPunt.getValue();
				objFila[0]= punt.getNom();
				objFila[1]= punt.getDescripcio();
				objFila[2]= punt.getGastronomic();
				objFila[3]= punt.getCultural();
				objFila[4]= punt.getOci();
				objFila[5]= punt.getEsport();
				objFila[6]= punt.getPlatja();
				objFila[7]= punt.getMuntanya();
				objFila[8]= punt.getRural();
				objFila[9]= punt.getEsqui();
				objFila[10]= punt.getNatacio();
				objFila[11]= punt.getEscalada();
				objFila[12]= punt.getCompres();
				objFila[13]= punt.getEquitacio();
				objFila[14]= punt.getSenderisme();
				model.addRow(objFila);
				
				
			}
			
			
		}
	
	

	
	}
}
