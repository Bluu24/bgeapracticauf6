package es.almata.precentacio;

import java.awt.BorderLayout;
import java.util.Map;

import javax.swing.JInternalFrame;

import es.almata.precentacio.ControladorPre;
import es.almata.domini.Destinacio;
import es.almata.domini.Punt_Interes;
import es.almata.persistencia.HelperBBDDMem;


public class ControladorPre {
	
	private static Aplicacio aplicacio=null;
	private static JInternalFrame pantallaActual=null;
	
	public static void crearAplicacio() { 
		if(aplicacio==null) {
			aplicacio= new Aplicacio();
		}
	}
	
	public static void canviPantalla(JInternalFrame SeguentPantalla) {
		if(pantallaActual!=null) 
			pantallaActual.dispose();
		aplicacio.getContentPane().add(SeguentPantalla, BorderLayout.CENTER);
		aplicacio.setVisible(true);
		pantallaActual=SeguentPantalla;
		
	}

	// METODES DE NEGOCI DELEGANT AL HELPER
	
	public static void addDesti(String provincia, String comarca, String ciutat_poblacio, int cp, boolean platja,
			boolean muntanya) {
		HelperBBDDMem.addDesti(new Destinacio(provincia,comarca,ciutat_poblacio,cp,platja,muntanya));
		
	}
	
	public static Map<String,Destinacio> getAllDestins(){
		return HelperBBDDMem.getAllDestins();
	}
	
	public static  Destinacio getDesti (String nomCiutat) {
		   return HelperBBDDMem.getDesti(nomCiutat);
		}
	
	public static void modificarDesti (String provincia,String comarca,String ciutat_poblacio,int cp,boolean platja,boolean muntanya) {
		HelperBBDDMem.modificarDesti(new Destinacio(provincia,comarca,ciutat_poblacio,cp, platja,muntanya ));
		
	}
	
	public static void removeDesti(String nomCiutat) {
		HelperBBDDMem.removeDesti(nomCiutat);
	
	}
	
	//------------------------------
	
	public static void addPuntInt(int ID,String nom, String descripcio, String imatge, boolean platja, boolean muntanya,
			boolean gastronomic, boolean cultural, boolean oci, boolean esport, boolean rural, boolean esqui,
			boolean natacio, boolean escalada, boolean compres, boolean equitacio, boolean senderisme, String ciutat_poblacio) {
		
		Punt_Interes puntInt= new Punt_Interes(ID,nom,descripcio,imatge,platja,muntanya,gastronomic,cultural,oci,esport,rural,esqui,natacio,escalada,compres,equitacio,senderisme);
		puntInt.setfk_Destinacio(ciutat_poblacio);
		HelperBBDDMem.addPunInt(puntInt);
		
		
	}
	
	public static Map<String,Punt_Interes> getAllPuntI(String nomCiutat){
		return HelperBBDDMem.getAllPuntI(nomCiutat);
	}
	
	public static Map<String,Punt_Interes> getAllPuntI2(){
		return HelperBBDDMem.getAllPuntI2();
	}
	
	public static  Punt_Interes getPuntInt (String nom) {
		   return HelperBBDDMem.getPuntInt(nom);
		}
	
	public static void removePuntInt(int id) {
		HelperBBDDMem.removePuntInt(id);
	
	}
	
	public static void modificarPuntInt(int ID,String nom, String descripcio, String imatge, boolean platja, boolean muntanya,
			boolean gastronomic, boolean cultural, boolean oci, boolean esport, boolean rural, boolean esqui,
			boolean natacio, boolean escalada, boolean compres, boolean equitacio, boolean senderisme, String ciutat_poblacio) {
		
		Punt_Interes puntInt= new Punt_Interes(ID,nom,descripcio,imatge,platja,muntanya,gastronomic,cultural,oci,esport,rural,esqui,natacio,escalada,compres,equitacio,senderisme);
		puntInt.setfk_Destinacio(ciutat_poblacio);
		HelperBBDDMem.modificarPuntInt(puntInt);
		
		
	}
	
/*

	public static  Destinacio getDesti (String nomCiutat) {
	   return HelperBBDDMem.getDesti(nomCiutat);
	}
	
	public static void removeDesti(String nomCiutat) {
		HelperBBDDMem.removeDesti(nomCiutat);
	
	}	
	
	
	public static void modificarDesti (String provincia,String comarca,String ciutat_poblacio,int cp,boolean platja,
			boolean muntanya) {
		HelperBBDDMem.modificarDesti(new Destinacio(provincia,comarca,ciutat_poblacio,cp, platja,muntanya ));
		
	}
	
	public static Map<String,Destinacio> getAllDestins(){
		return HelperBBDDMem.getAllDestins();
	}
	
	
	

	*/
	
	
	
	
	

}
