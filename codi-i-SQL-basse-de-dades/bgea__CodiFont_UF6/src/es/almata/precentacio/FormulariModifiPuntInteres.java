package es.almata.precentacio;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import es.almata.precentacio.ControladorPre;
import es.almata.domini.Destinacio;
import es.almata.domini.Punt_Interes;
import es.almata.utils.Fonts;
import es.almata.utils.Util;

public class FormulariModifiPuntInteres extends JInternalFrame implements Formulari{
	
	private static final String ID = "ID: ";
	private static final String TRIA_EL_PUNT_INT = "TRIA EL PUNT D'INTERES ";
	private static final String BOTO_CANCELAR = "Cancelar";
	private static final String BOTO_MODIFI = "Acceptar";
	private static final String RURAL = "Rural";
	private static final String MUNTANYA = "Muntanya";
	private static final String PLATJA = "Platja";
	private static final String ESPORT = "Esport";
	private static final String OCI = "Oci";
	private static final String CULTURAL = "Cultural";
	private static final String GASTRONÒMIC = "Gastronòmic";
	private static final String TIPUS = "Tipus: ";
	private static final String IMATGE = "Imatge";
	private static final String NOM = "Nom: ";
	
	private static final long serialVersionUID = 1L;

	private GridBagLayout layout;
	private Controlador controlador;
	private FormulariModifiPuntInteres fmp;
	
	//---------------------------
		private JButton btnImantge;
		private JLabel lblImatge;
		//---------------------------
		private JLabel lblNom;
		private JTextField txtNom;
		//---------------------------
		private JLabel lblId;
		private JTextField txtId;
		//---------------------------
		private JComboBox<String> cmbPunt;
		//---------------------------
		private JPanel pnlBotonsTipus;
		private JRadioButton rbtGastronomic;
		private JRadioButton rbtCultural;
		private JRadioButton rbtOci;
		private JRadioButton rbtEsport;
		private JRadioButton rbtPlatja;
		private JRadioButton rbtMuntanya;
		private JRadioButton rbtRural;
		private JLabel lblTipus;
		private ButtonGroup grupBotonsT;
		
		//---------------------------
		private JPanel pnlBotonsActivitats;
		private JCheckBox chbEsqui;
		private JCheckBox chbNatacio;
		private JCheckBox chbEscalada;
		private JCheckBox chbCompres;
		private JCheckBox chbEquitacio;
		private JCheckBox chbSenderisme;
		private JLabel lblActivitats;
		
		
		//---------------------------
		private JLabel lblDescripcio;
		private JTextArea txtDescripcio;
		private JScrollPane slpDescripcio;
		
		//---------------------------
		
		private JButton btnModificar;
		private JButton btnCancelar;
		private JPanel pnlBotons;
		
		//---------------------------
	
	public FormulariModifiPuntInteres() {
		inicialitzacions();
		crearComponents();
		afegirComponenets();
		posicionarComponents();
		setVisible(true);
	}
	
	@Override
	public void inicialitzacions() {
		Container c=this.getContentPane();
		((JComponent) c).setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GREEN),"Modificacio del Punt d'Interes",TitledBorder.CENTER,TitledBorder.DEFAULT_JUSTIFICATION,Fonts.fontTitol(),Color.GREEN));
		
		getContentPane().setLayout(layout= new GridBagLayout());
		controlador= new Controlador();
		fmp=this;
		
		Util.treureBarraTitolInteralFrame(this);
		
	}

	@Override
	public void crearComponents() {
		btnImantge = new JButton(IMATGE);
		lblImatge = new JLabel();
		lblImatge.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		btnImantge.addActionListener(controlador);
		btnImantge.setActionCommand(IMATGE);
		btnImantge.setEnabled(false);
		//---------------------------
		lblNom = new JLabel(NOM);
		txtNom = new JTextField();
		txtNom.setEnabled(false);
		//---------------------------
		lblId = new JLabel(ID);
		txtId = new JTextField();
		txtId.setEnabled(false);
		//---------------------------
		pnlBotonsTipus= new JPanel(new GridLayout(4,3));
		lblTipus = new JLabel(TIPUS);
		rbtGastronomic= new JRadioButton(GASTRONÒMIC);
		rbtGastronomic.setEnabled(false);
		
		rbtCultural= new JRadioButton(CULTURAL);
		rbtCultural.setEnabled(false);
		
		rbtOci= new JRadioButton(OCI);
		rbtOci.setEnabled(false);
		
		rbtEsport= new JRadioButton(ESPORT);
		rbtEsport.setEnabled(false);
		
		rbtPlatja= new JRadioButton(PLATJA);
		rbtPlatja.setEnabled(false);
		
		rbtMuntanya= new JRadioButton(MUNTANYA);
		rbtMuntanya.setEnabled(false);
		
		rbtRural= new JRadioButton(RURAL);
		rbtRural.setEnabled(false);
		
		grupBotonsT= new ButtonGroup();
		grupBotonsT.add(rbtGastronomic);
		grupBotonsT.add(rbtCultural);
		grupBotonsT.add(rbtOci);
		grupBotonsT.add(rbtEsport);
		grupBotonsT.add(rbtPlatja);
		grupBotonsT.add(rbtMuntanya);
		grupBotonsT.add(rbtRural);
		
		pnlBotonsTipus.add(rbtGastronomic);
		pnlBotonsTipus.add(rbtCultural);
		pnlBotonsTipus.add(rbtOci);
		pnlBotonsTipus.add(rbtEsport);
		pnlBotonsTipus.add(rbtPlatja);
		pnlBotonsTipus.add(rbtMuntanya);
		pnlBotonsTipus.add(rbtRural);
		
		pnlBotonsTipus.setEnabled(false);
		//---------------------------
		
		lblActivitats= new JLabel("Activitats: ");
		pnlBotonsActivitats= new JPanel(new GridLayout(3,3));
		
		chbEsqui= new JCheckBox("Esqui");
		chbEsqui.setEnabled(false);
		
		chbNatacio= new JCheckBox("Natació");
		chbNatacio.setEnabled(false);
		
		chbEscalada= new JCheckBox("Escalada");
		chbEscalada.setEnabled(false);
		
		chbCompres= new JCheckBox("Compres");
		chbCompres.setEnabled(false);
		
		chbEquitacio= new JCheckBox("Equitació");
		chbEquitacio.setEnabled(false);
		
		chbSenderisme= new JCheckBox("Senderisme");
		chbSenderisme.setEnabled(false);
		
		pnlBotonsActivitats.add(chbEsqui);
		pnlBotonsActivitats.add(chbNatacio);
		pnlBotonsActivitats.add(chbEscalada);
		pnlBotonsActivitats.add(chbCompres);
		pnlBotonsActivitats.add(chbEquitacio);
		pnlBotonsActivitats.add(chbSenderisme);

		//---------------------------
		
		lblDescripcio= new JLabel("Descripció: ");
		txtDescripcio= new JTextArea();
		txtDescripcio.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		txtDescripcio.setEnabled(false);
		
		slpDescripcio= new JScrollPane(txtDescripcio);
		slpDescripcio.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		slpDescripcio.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		slpDescripcio.setEnabled(false);
		//---------------------------
		
		pnlBotons= new JPanel();
		btnModificar = new JButton(BOTO_MODIFI);
		btnCancelar = new JButton(BOTO_CANCELAR);
		btnModificar = new JButton(BOTO_MODIFI);
		btnModificar.addActionListener(controlador);
		btnModificar.setActionCommand(BOTO_MODIFI);
		btnCancelar = new JButton(BOTO_CANCELAR);
		btnCancelar.addActionListener(controlador);
		btnCancelar.setActionCommand(BOTO_CANCELAR);
		pnlBotons.add(btnModificar);
		pnlBotons.add(btnCancelar);
		
		//---------------------------
		
		
		cmbPunt= new JComboBox<String>(omplirCombo());
		cmbPunt.setSelectedIndex(0);
		cmbPunt.setActionCommand("combo");
		cmbPunt.addActionListener(controlador);
		
	}
	
	
	private Vector<String> omplirCombo(){
		
		Map<String,Punt_Interes> puntInt= ControladorPre.getAllPuntI2();
		Vector<String> dadesCombo= new Vector<String>();
		dadesCombo.add(TRIA_EL_PUNT_INT);
		for(Entry<String,Punt_Interes> key: puntInt.entrySet()) {
			dadesCombo.add(key.getValue().getNom());
		}
		return dadesCombo;
	}
	//---------------------------

	@Override
	public void afegirComponenets() {
		this.getContentPane().add(cmbPunt);
//		---------------------------
		this.getContentPane().add(btnImantge);
		this.getContentPane().add(lblImatge);
//		---------------------------
		this.getContentPane().add(lblNom);
		this.getContentPane().add(txtNom);
//		---------------------------
		this.getContentPane().add(txtId);
		this.getContentPane().add(lblId);
//		---------------------------
		this.getContentPane().add(lblTipus);
		this.getContentPane().add(pnlBotonsTipus);
//		---------------------------
		this.getContentPane().add(pnlBotonsActivitats);
		this.getContentPane().add(lblActivitats);
//		---------------------------	
		this.getContentPane().add(lblDescripcio);
		this.getContentPane().add(slpDescripcio);
//		---------------------------	
		this.getContentPane().add(pnlBotons);
		
	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc= new GridBagConstraints();
		
		
		//RESTRINCCIONS DEL COMBO 
		gbc.gridx=0;//columna
		gbc.gridy=0;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(cmbPunt,gbc);
		
		//RESTRINCCIONS DE LA ETIQUETA (id)
		gbc.gridx=0;//columna
		gbc.gridy=1;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(lblId,gbc);
				
		//RESTRINCCIONS DEL QUADRO DE TEXT ID 
		gbc.gridx=1;//columna
		gbc.gridy=1;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		//gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.HORIZONTAL;//s'adapti al canvi de mesura de la pestanya
		layout.setConstraints(txtId,gbc);
	
		
		//RESTRINCCIONS DE LA ETIQUETA (Nom)
		gbc.gridx=0;//columna
		gbc.gridy=2;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		//gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(lblNom,gbc);
				
		//RESTRINCCIONS DEL QUADRO DE TEXT NOM 
		gbc.gridx=1;//columna
		gbc.gridy=2;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		//gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.HORIZONTAL;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(txtNom,gbc);
		
		//RESTRINCCIONS DEL LA ETIQUETA TIPUS
		gbc.gridx=0;//columna
		gbc.gridy=3;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		//gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(lblTipus,gbc);
						
		//RESTRINCCIONS DEL PANEL tipus
		gbc.gridx=1;//columna
		gbc.gridy=4;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		//gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(pnlBotonsTipus,gbc);
		
		//RESTRINCCIONS DE LA ETIQUETA (Activitats)
		gbc.gridx=0;//columna
		gbc.gridy=7;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		//gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(lblActivitats,gbc);
						
		//RESTRINCCIONS DEL PANEL Activitats
		gbc.gridx=1;//columna
		gbc.gridy=8;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		//gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(pnlBotonsActivitats,gbc);
		
		//RESTRINCCIONS DE LA ETIQUETA (DESCRIPCIÓ)
		gbc.gridx=0;//columna
		gbc.gridy=11;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		//gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(lblDescripcio,gbc);
		
		//RESTRINCCIONS DEL PANEL DESCRIPCIÓ
		gbc.gridx=0;//columna
		gbc.gridy=13;//fila
		gbc.gridheight=2;//alçada en files
		gbc.gridwidth= 2;//amplada en columnes
		gbc.weightx= 0.5;//creixement columna
		gbc.weighty= 0.5;//creixement fila
		gbc.fill=GridBagConstraints.BOTH;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(slpDescripcio,gbc);
				
		//RESTRINCCIONS DEL BOTO...
		gbc.gridx=0;//columna
		gbc.gridy=16;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(btnImantge,gbc);
					
		//RESTRINCCIONS DE LA ETIQUETA (imatge)...
		gbc.gridx=0;//columna
		gbc.gridy=18;//fila
		gbc.gridheight=3;//alçada en files
		gbc.gridwidth= 3;//amplada en columnes
		gbc.weightx= 0.5;//creixement columna
		gbc.weighty= 0.5;//creixement fila	
		gbc.fill=GridBagConstraints.BOTH;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(lblImatge,gbc);
		
		//RESTRINCCIONS DEL PANEL
		gbc.gridx=1;//columna
		gbc.gridy=22;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.EAST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(pnlBotons,gbc);
		
	}
	
	public class Controlador implements ActionListener{
		String cami;
		String nom;
		Destinacio destinacio;
		Punt_Interes puntInt;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj=e.getSource();
			
			if(obj instanceof JButton) {
				JButton boto= (JButton) obj;
				if(boto.getActionCommand().equals(BOTO_MODIFI)) {
					System.out.println("He clicat el botó acceptar!!");	
					if(dadesCorrectes()) {
						
						int tempint;
						try {
						tempint=Integer.parseInt(txtId.getText());
						ControladorPre.modificarPuntInt(tempint, txtNom.getText(), txtDescripcio.getText(), cami,rbtPlatja.isSelected(),rbtMuntanya.isSelected(),rbtGastronomic.isSelected(),rbtCultural.isSelected(),rbtOci.isSelected(),rbtEsport.isSelected(),rbtRural.isSelected(),chbEsqui.isSelected(),chbNatacio.isSelected(),chbEscalada.isSelected(), chbCompres.isSelected(), chbEquitacio.isSelected(), chbSenderisme.isSelected(),(String)cmbPunt.getSelectedItem());
						
						
						
						cmbPunt.setFocusable(true);
						
						}catch(NumberFormatException nfe) {
							
						}
						
					}
					
					
				}else if(boto.getActionCommand().equals(BOTO_CANCELAR)) {
					System.out.println("He clicat el botó cancel·lar!!");
					ControladorPre.canviPantalla(new FormulariDefault());
				}else if(boto.getActionCommand().equals(IMATGE)) {
					carregaImetge();
				}
			}else if(obj instanceof JComboBox<?>) {
				if(cmbPunt.getActionCommand().equals("combo")) {
					omplirFormulariPuntsIntSeleccionat();
					btnImantge.setEnabled(true);
					txtNom.setEnabled(true);
					rbtGastronomic.setEnabled(true);
					rbtCultural.setEnabled(true);
					rbtOci.setEnabled(true);
					rbtEsport.setEnabled(true);
					rbtPlatja.setEnabled(true);
					rbtMuntanya.setEnabled(true);
					rbtRural.setEnabled(true);
					chbEsqui.setEnabled(true);
					chbNatacio.setEnabled(true);
					chbEscalada.setEnabled(true);
					chbCompres.setEnabled(true);
					chbEquitacio.setEnabled(true);
					chbSenderisme.setEnabled(true);
					txtDescripcio.setEnabled(true);
					slpDescripcio.setEnabled(true);
					
				}
			}		
		}
		
		
		private void omplirFormulariPuntsIntSeleccionat() {
			if(cmbPunt.getSelectedIndex()!=0) {
				String nom= String.valueOf( cmbPunt.getSelectedItem());
				System.out.println("Nom Punt d'Interres selecionat combo: "+ nom);
				afegirPuntInteresFormulari();	
				
			}
			
		}
		
		
		private void afegirPuntInteresFormulari() {
			
			
			txtId.setText(String.valueOf(puntInt.getID()));
			txtNom.setText(puntInt.getNom());
			txtDescripcio.setText(puntInt.getDescripcio());
			
			ImageIcon imatge= Util.redimensionarImatge(puntInt.getImatge(), lblImatge.getWidth(), lblImatge.getHeight());
			lblImatge.setIcon(imatge);
			
			rbtGastronomic.setSelected(puntInt.getGastronomic());
			rbtCultural.setSelected(puntInt.getCultural());
			rbtOci.setSelected(puntInt.getOci());
			rbtEsport.setSelected(puntInt.getEsport());
			rbtPlatja.setSelected(puntInt.getPlatja());
			rbtMuntanya.setSelected(puntInt.getMuntanya());
			rbtRural.setSelected(puntInt.getRural());
			
			
			chbEsqui.setSelected(puntInt.getEsqui());
			chbNatacio.setSelected(puntInt.getNatacio());
			chbEscalada.setSelected(puntInt.getEscalada());
			chbCompres.setSelected(puntInt.getCompres());
			chbEquitacio.setSelected(puntInt.getEquitacio());
			chbSenderisme.setSelected(puntInt.getSenderisme());
			
			
			
			
			
			
			
			

			
			
		}
		

		private boolean dadesCorrectes() {
			boolean correcte= true;
			if(txtId.getText().isEmpty()) {
				Object[]objTExtBotons= {"Acceptar"};
				
				JOptionPane.showOptionDialog(fmp, "Falte Intoduir la ID", "IMPORTANT", JOptionPane.OK_OPTION , JOptionPane.WARNING_MESSAGE, null, objTExtBotons, null);
				correcte= false;
			}
			return correcte;
		}


		private void carregaImetge() {
			
			JFileChooser selector= new JFileChooser();
			
			selector.setAcceptAllFileFilterUsed(false);
			FileNameExtensionFilter filtre= new FileNameExtensionFilter("Imatge","png","jpg","gif");
			selector.addChoosableFileFilter(filtre);
	
			int estat= selector.showOpenDialog(fmp);
		
			File arxiu= selector.getSelectedFile();
			if(arxiu!=null) {
				cami = arxiu.getAbsolutePath();
				if(!arxiu.exists()) {
					System.out.println("El fitxe no existeix !!!");
				}else if (estat== JFileChooser.APPROVE_OPTION) {
					ImageIcon imatge= Util.redimensionarImatge(cami, lblImatge.getWidth(), lblImatge.getHeight());
					lblImatge.setIcon(imatge);
				}
			}	
		}
		

		
	}
}
