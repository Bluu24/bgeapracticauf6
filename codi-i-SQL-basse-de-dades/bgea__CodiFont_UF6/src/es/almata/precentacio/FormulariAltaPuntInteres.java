package es.almata.precentacio;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;


import es.almata.domini.Destinacio;
import es.almata.domini.Punt_Interes;
import es.almata.exepccions.GestorExepcions;
import es.almata.utils.Fonts;
import es.almata.utils.Util;

public class FormulariAltaPuntInteres extends JInternalFrame implements Formulari{
	
	private static final String ID = "ID: ";
	private static final String TIRIA_EL_DESTI = "TRIA EL DESTI ";
	private static final String BOTO_CANCELAR = "Cancelar";
	private static final String BOTO_ACCEPTAR = "Acceptar";
	private static final String RURAL = "Rural";
	private static final String MUNTANYA = "Muntanya";
	private static final String PLATJA = "Platja";
	private static final String ESPORT = "Esport";
	private static final String OCI = "Oci";
	private static final String CULTURAL = "Cultural";
	private static final String GASTRONÒMIC = "Gastronòmic";
	private static final String TIPUS = "Tipus: ";
	private static final String IMATGE = "Imatge";
	private static final String NOM = "Nom: ";

	private static final long serialVersionUID = 1L;
	
	private GridBagLayout layout;
	private Controlador controlador;
	private FormulariAltaPuntInteres fap;
	
	//---------------------------
	private JButton btnImantge;
	private JLabel lblImatge;
	//---------------------------
	private JLabel lblNom;
	private JTextField txtNom;
	//---------------------------
	private JComboBox<String> cmbDesti;
	//---------------------------
	private JLabel lblId;
	private JTextField txtId;
	//---------------------------

	private JPanel pnlBotonsTipus;
	private JRadioButton rbtGastronomic;
	private JRadioButton rbtCultural;
	private JRadioButton rbtOci;
	private JRadioButton rbtEsport;
	private JRadioButton rbtPlatja;
	private JRadioButton rbtMuntanya;
	private JRadioButton rbtRural;
	private JLabel lblTipus;
	private ButtonGroup grupBotonsT;
	
	//---------------------------
	private JPanel pnlBotonsActivitats;
	private JCheckBox chbEsqui;
	private JCheckBox chbNatacio;
	private JCheckBox chbEscalada;
	private JCheckBox chbCompres;
	private JCheckBox chbEquitacio;
	private JCheckBox chbSenderisme;
	private JLabel lblActivitats;
	
	
	//---------------------------
	private JLabel lblDescripcio;
	private JTextArea txtDescripcio;
	private JScrollPane slpDescripcio;
	
	//---------------------------
	
	private JButton btnAcceptar;
	private JButton btnCancelar;
	private JPanel pnlBotons;
	
	//---------------------------

	
	public FormulariAltaPuntInteres() {
		inicialitzacions();
		crearComponents();
		afegirComponenets();
		posicionarComponents();
		setVisible(true);
	}
	
	
	
	
	@Override
	public void inicialitzacions() {
		
		Container c=this.getContentPane();
		((JComponent) c).setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE),"Alta del Punt d'Interes",TitledBorder.CENTER,TitledBorder.DEFAULT_JUSTIFICATION,Fonts.fontTitol(),Color.BLUE));
		
		getContentPane().setLayout(layout= new GridBagLayout());
		controlador= new Controlador();
		fap=this;
		
		Util.treureBarraTitolInteralFrame(this);
		
	}

	@Override
	public void crearComponents() {
		
		btnImantge = new JButton(IMATGE);
		lblImatge = new JLabel();
		lblImatge.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		btnImantge.addActionListener(controlador);
		btnImantge.setActionCommand(IMATGE);
		btnImantge.setEnabled(false);

		//---------------------------
		lblNom = new JLabel(NOM);
		txtNom = new JTextField();
		txtNom.setEnabled(false);
		//---------------------------
		lblId = new JLabel(ID);
		txtId = new JTextField();
		txtId.setEnabled(false);
		//---------------------------
		pnlBotonsTipus= new JPanel(new GridLayout(4,3));
		lblTipus = new JLabel(TIPUS);
		
		rbtGastronomic= new JRadioButton(GASTRONÒMIC);
		rbtGastronomic.setEnabled(false);
		
		rbtCultural= new JRadioButton(CULTURAL);
		rbtCultural.setEnabled(false);
		
		rbtOci= new JRadioButton(OCI);
		rbtOci.setEnabled(false);
		
		rbtEsport= new JRadioButton(ESPORT);
		rbtEsport.setEnabled(false);
		
		rbtPlatja= new JRadioButton(PLATJA);
		rbtPlatja.setEnabled(false);
		
		rbtMuntanya= new JRadioButton(MUNTANYA);
		rbtMuntanya.setEnabled(false);
		
		rbtRural= new JRadioButton(RURAL);
		rbtRural.setEnabled(false);
		
		grupBotonsT= new ButtonGroup();
		grupBotonsT.add(rbtGastronomic);
		grupBotonsT.add(rbtCultural);
		grupBotonsT.add(rbtOci);
		grupBotonsT.add(rbtEsport);
		grupBotonsT.add(rbtPlatja);
		grupBotonsT.add(rbtMuntanya);
		grupBotonsT.add(rbtRural);
		
		pnlBotonsTipus.add(rbtGastronomic);
		pnlBotonsTipus.add(rbtCultural);
		pnlBotonsTipus.add(rbtOci);
		pnlBotonsTipus.add(rbtEsport);
		pnlBotonsTipus.add(rbtPlatja);
		pnlBotonsTipus.add(rbtMuntanya);
		pnlBotonsTipus.add(rbtRural);
		
		pnlBotonsTipus.setEnabled(false);

		//---------------------------
		
		lblActivitats= new JLabel("Activitats: ");
		pnlBotonsActivitats= new JPanel(new GridLayout(3,3));
		
		chbEsqui= new JCheckBox("Esqui");
		chbEsqui.setEnabled(false);
		
		chbNatacio= new JCheckBox("Natació");
		chbNatacio.setEnabled(false);
		
		chbEscalada= new JCheckBox("Escalada");
		chbEscalada.setEnabled(false);
		
		chbCompres= new JCheckBox("Compres");
		chbCompres.setEnabled(false);
		
		chbEquitacio= new JCheckBox("Equitació");
		chbEquitacio.setEnabled(false);
		
		chbSenderisme= new JCheckBox("Senderisme");
		chbSenderisme.setEnabled(false);
		
		pnlBotonsActivitats.add(chbEsqui);
		pnlBotonsActivitats.add(chbNatacio);
		pnlBotonsActivitats.add(chbEscalada);
		pnlBotonsActivitats.add(chbCompres);
		pnlBotonsActivitats.add(chbEquitacio);
		pnlBotonsActivitats.add(chbSenderisme);
		

		//---------------------------
		
		lblDescripcio= new JLabel("Descripció: ");
		txtDescripcio= new JTextArea();
		txtDescripcio.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		txtDescripcio.setEnabled(false);

		
		slpDescripcio= new JScrollPane(txtDescripcio);
		slpDescripcio.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		slpDescripcio.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		slpDescripcio.setEnabled(false);

		
		//---------------------------
		
		pnlBotons= new JPanel();
		btnAcceptar = new JButton(BOTO_ACCEPTAR);
		btnAcceptar.addActionListener(controlador);
		btnAcceptar.setActionCommand(BOTO_ACCEPTAR);
		btnCancelar = new JButton(BOTO_CANCELAR);
		btnCancelar.addActionListener(controlador);
		btnCancelar.setActionCommand(BOTO_CANCELAR);
		pnlBotons.add(btnAcceptar);
		pnlBotons.add(btnCancelar);
		
		
		//---------------------------
		
		cmbDesti= new JComboBox<String>(omplirCombo());
		cmbDesti.setSelectedIndex(0);
		cmbDesti.setActionCommand("combo");
		cmbDesti.addActionListener(controlador);
	}

	

	private Vector<String> omplirCombo(){
		Map<String,Destinacio> desti= ControladorPre.getAllDestins();
		Vector<String> dadesCombo= new Vector<String>();
		dadesCombo.add(TIRIA_EL_DESTI);
		for(Entry<String,Destinacio> key: desti.entrySet()) {
			dadesCombo.add(key.getValue().getCiutat_poblacio());
		}
		return dadesCombo;
	}
	

	
	@Override
	public void afegirComponenets() {

		this.getContentPane().add(cmbDesti);
//		---------------------------
		this.getContentPane().add(btnImantge);
		this.getContentPane().add(lblImatge);
//		---------------------------
		this.getContentPane().add(lblNom);
		this.getContentPane().add(txtNom);
//		---------------------------
		this.getContentPane().add(txtId);
		this.getContentPane().add(lblId);
//		---------------------------
		this.getContentPane().add(lblTipus);
		this.getContentPane().add(pnlBotonsTipus);
//		---------------------------
		this.getContentPane().add(pnlBotonsActivitats);
		this.getContentPane().add(lblActivitats);
//		---------------------------	
		this.getContentPane().add(lblDescripcio);
		this.getContentPane().add(slpDescripcio);
//		---------------------------	
		this.getContentPane().add(pnlBotons);

	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc= new GridBagConstraints();
		
		//RESTRINCCIONS DEL COMBO
		gbc.gridx=0;//columna
		gbc.gridy=0;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(cmbDesti,gbc);
		
		//RESTRINCCIONS DE LA ETIQUETA (id)
		gbc.gridx=0;//columna
		gbc.gridy=1;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(lblId,gbc);
				
		//RESTRINCCIONS DEL QUADRO DE TEXT ID 
		gbc.gridx=1;//columna
		gbc.gridy=1;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		//gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.HORIZONTAL;//s'adapti al canvi de mesura de la pestanya
		layout.setConstraints(txtId,gbc);
		
		//RESTRINCCIONS DE LA ETIQUETA (Nom)
		gbc.gridx=0;//columna
		gbc.gridy=2;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(lblNom,gbc);
				
		//RESTRINCCIONS DEL QUADRO DE TEXT NOM 
		gbc.gridx=1;//columna
		gbc.gridy=2;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		//gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.HORIZONTAL;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(txtNom,gbc);
		
		//RESTRINCCIONS DEL LA ETIQUETA TIPUS
		gbc.gridx=0;//columna
		gbc.gridy=3;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		//gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(lblTipus,gbc);
						
		//RESTRINCCIONS DEL PANEL tipus
		gbc.gridx=1;//columna
		gbc.gridy=4;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		//gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(pnlBotonsTipus,gbc);
		
		//RESTRINCCIONS DE LA ETIQUETA (Activitats)
		gbc.gridx=0;//columna
		gbc.gridy=7;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		//gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(lblActivitats,gbc);
						
		//RESTRINCCIONS DEL PANEL Activitats
		gbc.gridx=1;//columna
		gbc.gridy=8;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		//gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(pnlBotonsActivitats,gbc);
		
		//RESTRINCCIONS DE LA ETIQUETA (DESCRIPCIÓ)
		gbc.gridx=0;//columna
		gbc.gridy=11;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		//gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(lblDescripcio,gbc);
		
		//RESTRINCCIONS DEL PANEL DESCRIPCIÓ
		gbc.gridx=0;//columna
		gbc.gridy=13;//fila
		gbc.gridheight=2;//alçada en files
		gbc.gridwidth= 2;//amplada en columnes
		gbc.weightx= 0.5;//creixement columna
		gbc.weighty= 0.5;//creixement fila
		gbc.fill=GridBagConstraints.BOTH;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(slpDescripcio,gbc);
				
		//RESTRINCCIONS DEL BOTO...
		gbc.gridx=0;//columna
		gbc.gridy=16;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(btnImantge,gbc);
					
		//RESTRINCCIONS DE LA ETIQUETA (imatge)...
		gbc.gridx=0;//columna
		gbc.gridy=18;//fila
		gbc.gridheight=3;//alçada en files
		gbc.gridwidth= 3;//amplada en columnes
		gbc.weightx= 0.5;//creixement columna
		gbc.weighty= 0.5;//creixement fila	
		gbc.fill=GridBagConstraints.BOTH;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(lblImatge,gbc);
		
		//RESTRINCCIONS DEL PANEL
		gbc.gridx=1;//columna
		gbc.gridy=22;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.EAST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(pnlBotons,gbc);
				
		
	}

	public class Controlador implements ActionListener{
		String cami;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj=e.getSource();

			if(obj instanceof JButton) {
				JButton boto= (JButton) obj;
				if(boto.getActionCommand().equals(BOTO_ACCEPTAR)) {
					System.out.println("He clicat el botó acceptar!!");	
					
					if(dadesCorrectes()) {
						try {
							int tempint;
							try {
							tempint=Integer.parseInt(txtId.getText());
							ControladorPre.addPuntInt(tempint, txtNom.getText(), txtDescripcio.getText(), cami,rbtPlatja.isSelected(),rbtMuntanya.isSelected(),rbtGastronomic.isSelected(),rbtCultural.isSelected(),rbtOci.isSelected(),rbtEsport.isSelected(),rbtRural.isSelected(),chbEsqui.isSelected(),chbNatacio.isSelected(),chbEscalada.isSelected(), chbCompres.isSelected(), chbEquitacio.isSelected(), chbSenderisme.isSelected(),(String)cmbDesti.getSelectedItem());
							
							netejarFormulari();
							
							cmbDesti.setFocusable(true);
							}catch(NumberFormatException nfe) {
								Object[]objTExtBotons= {"Acceptar"};
								
								JOptionPane.showOptionDialog(fap, "La ID ha de ser un numero enter", "IMPORTANT", JOptionPane.OK_OPTION , JOptionPane.WARNING_MESSAGE, null, objTExtBotons, null);
							}
						}catch(GestorExepcions ge) {

							Object[]objTExtBotons= {"Acceptar"};
							
							JOptionPane.showOptionDialog(fap, "Error SQL: "+ge.getObjecteExepcio().getMessage()+"\nClasse: "+ge.getNomClasse(), "FINESTRA D'INFORMACIÓ", JOptionPane.OK_OPTION , JOptionPane.WARNING_MESSAGE, null, objTExtBotons, objTExtBotons[0]);
							
							Object[]objTExtBotons2= {"Acceptar"};
							
							JOptionPane.showOptionDialog(fap, "Error Coneccio: "+ge.getObjecteExepcio().getMessage()+"\nClasse: "+ge.getNomClasse(), "FINESTRA D'INFORMACIÓ", JOptionPane.OK_OPTION , JOptionPane.WARNING_MESSAGE, null, objTExtBotons2, objTExtBotons2[0]);
						
						}
					}
					
					
				}else if(boto.getActionCommand().equals(BOTO_CANCELAR)) {
					System.out.println("He clicat el botó cancel·lar!!");
					ControladorPre.canviPantalla(new FormulariDefault());
				}else if(boto.getActionCommand().equals(IMATGE)) {
					carregaImetge();
				}	
			}else if(obj instanceof JComboBox<?>) {
				if(cmbDesti.getActionCommand().equals("combo")) {
					btnImantge.setEnabled(true);
					txtNom.setEnabled(true);
					txtId.setEnabled(true);
					rbtGastronomic.setEnabled(true);
					rbtCultural.setEnabled(true);
					rbtOci.setEnabled(true);
					rbtEsport.setEnabled(true);
					rbtPlatja.setEnabled(true);
					rbtMuntanya.setEnabled(true);
					rbtRural.setEnabled(true);
					chbEsqui.setEnabled(true);
					chbNatacio.setEnabled(true);
					chbEscalada.setEnabled(true);
					chbCompres.setEnabled(true);
					chbEquitacio.setEnabled(true);
					chbSenderisme.setEnabled(true);
					txtDescripcio.setEnabled(true);
					slpDescripcio.setEnabled(true);
				}
			}
		}

		private void netejarFormulari() {
			txtNom.setText("");
			txtId.setText("");
			rbtGastronomic.setSelected(false);
			rbtCultural.setSelected(false);
			rbtOci.setSelected(false);
			rbtEsport.setSelected(false);
			rbtPlatja.setSelected(false);
			rbtMuntanya.setSelected(false);
			rbtRural.setSelected(false);
			chbEsqui.setSelected(false);
			chbNatacio.setSelected(false);
			chbEscalada.setSelected(false);
			chbCompres.setSelected(false);
			chbEquitacio.setSelected(false);
			chbSenderisme.setSelected(false);
			txtDescripcio.setText("");
			
		}

		private boolean dadesCorrectes() {
			boolean correcte= true;
			if(txtId.getText().isEmpty()) {
				Object[]objTExtBotons= {"Acceptar"};
				
				JOptionPane.showOptionDialog(fap, "Falte Intoduir la ID", "IMPORTANT", JOptionPane.OK_OPTION , JOptionPane.WARNING_MESSAGE, null, objTExtBotons, null);
				correcte= false;
			}
			return correcte;
		}

		
		private void carregaImetge() {
			
			JFileChooser selector= new JFileChooser();
			
			selector.setAcceptAllFileFilterUsed(false);
			FileNameExtensionFilter filtre= new FileNameExtensionFilter("Imatge","png","jpg","gif");
			selector.addChoosableFileFilter(filtre);
			//navegar pel disc
			int estat= selector.showOpenDialog(fap);
			
		
			File arxiu= selector.getSelectedFile();
			if(arxiu!=null) {
				cami = arxiu.getAbsolutePath();
				if(!arxiu.exists()) {
					System.out.println("El fitxe no existeix !!!");
				}else if (estat== JFileChooser.APPROVE_OPTION) {
					ImageIcon imatge= Util.redimensionarImatge(cami, lblImatge.getWidth(), lblImatge.getHeight());
					lblImatge.setIcon(imatge);
				}
			}	
		}
	}	
}

