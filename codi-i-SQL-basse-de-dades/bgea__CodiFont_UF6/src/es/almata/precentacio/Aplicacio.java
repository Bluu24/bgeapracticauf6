package es.almata.precentacio;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;


import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.KeyStroke;



public class Aplicacio extends JFrame {

	
	private static final String ENCRIPTAT_COMPROVACIO = "Encriptat Comprovacio";
	private static final String MODIFICACIO_DESTINACIÓ = "Modificacio Destinació";
	public static final int ALCADA = 800;
	public static final int AMPLADA = 1000;
	private static final String MODIFICACIO_PI = "Modificacio Punts d'Interes";
	private static final String CONSULTA_DES = "Consulta Destinació";
	private static final String CONSULTA_PI = "Consulta Punts d'Interes";
	private static final String CONSULTA = "Consulta General";
	private static final String BAIXA_DES = "Baixa Destinació";
	private static final String BAIXA_PI = "Baixa Punts d'Interes";
	private static final String ALTA_DES = "Alta Destincació";
	private static final String ALTA_PUNTS_D_INTERES = "Alta Punts d'Interes";
	private static final String GESTIO = "Gestio";
	
	private JMenu gestio;
	private JMenuItem altaDestincacio;
	private JMenuItem altaPuntInteres;
	private JMenuItem baixaDestincacio;
	private JMenuItem baixaPuntInteres;
	private JMenuItem modificacioDestincacio;
	private JMenuItem modificacioPuntInteres;
	private JMenuItem consultaDestincacio;
	private JMenuItem consultaPuntInteres;
	private JMenuItem consulta;
	private JMenuItem comprovarenctriptat;
	private Controlador conrolador;
	

	
	
	private static final long serialVersionUID = 1L;
	private static final String TEXT_TITOL_FINESTRA = "APLICACIÓ GESTIÓ DE DESTINS I PUNTS D'INTERES";
	
	public Aplicacio() {
		inicialitzacions();
		this.setJMenuBar(menu());
	}

	private JMenuBar menu() {
		//contenidor del menú 
		JMenuBar barraMenu= new JMenuBar();
		 
	   
	    
		//opcions de menú 1
		gestio= new JMenu(GESTIO);
		
		barraMenu.add(gestio);
		
		ImageIcon imagen1=new ImageIcon("imatges/add.png");
		altaDestincacio= new JMenuItem(ALTA_DES);
		altaDestincacio.setIcon(imagen1);
		altaDestincacio.setMnemonic(KeyEvent.VK_D);
		altaDestincacio.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D,ActionEvent.ALT_MASK));
		altaDestincacio.setActionCommand("Alta Destincacio");
		altaDestincacio.addActionListener(conrolador);
		gestio.add(altaDestincacio);
		
		
		altaPuntInteres= new JMenuItem(ALTA_PUNTS_D_INTERES);
		altaPuntInteres.setIcon(imagen1);
		altaPuntInteres.setMnemonic(KeyEvent.VK_P);
		altaPuntInteres.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P,ActionEvent.ALT_MASK));
		altaPuntInteres.setActionCommand("Alta Punt d'Interes");
		altaPuntInteres.addActionListener(conrolador);
		//Afegir listener
		gestio.add(altaPuntInteres);
		
		gestio.addSeparator();//linia de separacio
		
		ImageIcon imagen2=new ImageIcon("imatges/delete.png");
		baixaDestincacio= new JMenuItem(BAIXA_DES);
		baixaDestincacio.setIcon(imagen2);
		baixaDestincacio.setMnemonic(KeyEvent.VK_D);
		baixaDestincacio.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D,ActionEvent.CTRL_MASK));
		baixaDestincacio.setActionCommand("Baixa Destinacio");
		baixaDestincacio.addActionListener(conrolador);
		//Afegir listener
		gestio.add(baixaDestincacio);
		
		
		baixaPuntInteres= new JMenuItem(BAIXA_PI);
		baixaPuntInteres.setIcon(imagen2);
		baixaPuntInteres.setMnemonic(KeyEvent.VK_P);
		baixaPuntInteres.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P,ActionEvent.CTRL_MASK));
		baixaPuntInteres.setActionCommand("Baixa Punts d'Interes");
		baixaPuntInteres.addActionListener(conrolador);
		//Afegir listener
		gestio.add(baixaPuntInteres);
	
		gestio.addSeparator();//linia de separacio
		
		ImageIcon imagen4=new ImageIcon("imatges/pencil.png");
		modificacioDestincacio= new JMenuItem(MODIFICACIO_DESTINACIÓ);
		modificacioDestincacio.setIcon(imagen4);
		modificacioDestincacio.setMnemonic(KeyEvent.VK_D);
		modificacioDestincacio.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D,ActionEvent.SHIFT_MASK));
		modificacioDestincacio.setActionCommand("Modificacio Destinacio");
		modificacioDestincacio.addActionListener(conrolador);
		//Afegir listener
		gestio.add(modificacioDestincacio);
		
		modificacioPuntInteres= new JMenuItem(MODIFICACIO_PI);
		modificacioPuntInteres.setIcon(imagen4);
		modificacioPuntInteres.setMnemonic(KeyEvent.VK_P);
		modificacioPuntInteres.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P,ActionEvent.SHIFT_MASK));
		modificacioPuntInteres.setActionCommand("Modificacio Punts d'Interes");
		modificacioPuntInteres.addActionListener(conrolador);
		//Afegir listener
		gestio.add(modificacioPuntInteres);
		
	
		gestio.addSeparator();//linia de separacio
		ImageIcon imagen3=new ImageIcon("imatges/find.png");
		consultaDestincacio= new JMenuItem(CONSULTA_DES);
		consultaDestincacio.setIcon(imagen3);
		consultaDestincacio.setMnemonic(KeyEvent.VK_D);
		consultaDestincacio.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D,ActionEvent.META_MASK));
		consultaDestincacio.setActionCommand("Consulta Destinacio");
		consultaDestincacio.addActionListener(conrolador);
		//Afegir listener
		gestio.add(consultaDestincacio);
		
		
		consultaPuntInteres= new JMenuItem(CONSULTA_PI);
		consultaPuntInteres.setIcon(imagen3);
		consultaPuntInteres.setMnemonic(KeyEvent.VK_P);
		consultaPuntInteres.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P,ActionEvent.META_MASK));
		consultaPuntInteres.setActionCommand("Consulta Punts d'Interes");
		consultaPuntInteres.addActionListener(conrolador);
		//Afegir listener
		gestio.add(consultaPuntInteres);
		
		gestio.addSeparator();//linia de separacio
		
		consulta= new JMenuItem(CONSULTA);
		consulta.setIcon(imagen3);
		consulta.setMnemonic(KeyEvent.VK_C);
		consulta.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.META_MASK));
		consulta.setActionCommand("Consulta General");
		consulta.addActionListener(conrolador);
		//Afegir listener
		gestio.add(consulta);
		

		gestio.addSeparator();//linia de separacio
		
		ImageIcon imagen5=new ImageIcon("imatges/inte.png");
		comprovarenctriptat= new JMenuItem(ENCRIPTAT_COMPROVACIO);
		comprovarenctriptat.setIcon(imagen5);
		comprovarenctriptat.setMnemonic(KeyEvent.VK_E);
		comprovarenctriptat.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E,ActionEvent.CTRL_MASK));
		comprovarenctriptat.setActionCommand("Encriptat");
		comprovarenctriptat.addActionListener(conrolador);
		//Afegir listener
		gestio.add(comprovarenctriptat);
	
		return barraMenu;
	}

	private void inicialitzacions() {
		this.setBounds(599, 50, AMPLADA, ALCADA);//mides finestra
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle(TEXT_TITOL_FINESTRA);
		this.setIconImage(new ImageIcon("imatges/icon.png").getImage());//icona de la imatge
		Container c=this.getContentPane();
		((JComponent) c).setBorder(BorderFactory.createEmptyBorder(5,5,5,5));//bordes 
		conrolador= new Controlador();
	}
	
	public class Controlador implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj= e.getSource(); //saver l'origen
			if(obj instanceof JCheckBoxMenuItem) {
				JCheckBoxMenuItem jcmi= (JCheckBoxMenuItem) obj;
				if(jcmi.isEnabled()) System.out.println("Catellà");
			}else if(obj instanceof JRadioButtonMenuItem) {
				JRadioButtonMenuItem jrmi= (JRadioButtonMenuItem) obj;
				if(jrmi.isEnabled()) System.out.println("Català");
			}else if(obj instanceof JMenuItem) {
				JMenuItem jmi= (JMenuItem) obj;
				switch (jmi.getActionCommand()) {
				case "Alta Destincacio":
					//System.out.println("Alta");
					ControladorPre.canviPantalla(new FormulariAltaDesti());
					break;
				case "Alta Punt d'Interes":
					ControladorPre.canviPantalla(new FormulariAltaPuntInteres());
					break;
				case "Baixa Destinacio":
					ControladorPre.canviPantalla(new FormulariBaixaDesti());
					break;
				case "Baixa Punts d'Interes":
					ControladorPre.canviPantalla(new FormulariBaixaPuntInteres());
					break;
				case "Modificacio Destinacio":
					ControladorPre.canviPantalla(new FormulariModifiDesti());
					break;
				case "Modificacio Punts d'Interes":
					ControladorPre.canviPantalla(new FormulariModifiPuntInteres());
					break;
				case "Consulta Destinacio":
					ControladorPre.canviPantalla(new FormulariConsuDesti());
					break;
				case "Consulta Punts d'Interes":
					ControladorPre.canviPantalla(new FormulariConsuPuntInteres());
					break;
				case "Consulta General":
					ControladorPre.canviPantalla(new FormulariConsulta());
					break;
				case "Encriptat":
					ControladorPre.canviPantalla(new FormulariComprovacioEncriptat());
					break;
				}
			}
			
		}
		
		
		
		
		
	}
		
		
		
		
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


