package es.almata.precentacio;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;
import java.util.Map.Entry;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

import es.almata.precentacio.ControladorPre;
import es.almata.domini.Destinacio;
import es.almata.domini.Punt_Interes;
import es.almata.utils.Fonts;
import es.almata.utils.Util;

public class FormulariConsuPuntInteres extends JInternalFrame implements Formulari {
	

	
	private static final String TRIA_EL_PUNT_INT = "TRIA PUNT D'INTERES  ";
	
	
	private static final long serialVersionUID = 1L;

	private GridBagLayout layout;
	private Controlador controlador;
	private FormulariConsuPuntInteres finestraPrincipal;
	
	//---------------------------
	
		private JComboBox<String> cmbPunt;
		private DefaultTableModel model;
		private JTable taula;
		private JLabel lbltaula;
		private JScrollPane slpTaula;
		private JButton btnAcceptar;
		private JPanel panell;
			
		//---------------------------
	
	
	
	public FormulariConsuPuntInteres() {
		inicialitzacions();
		crearComponents();
		afegirComponenets();
		posicionarComponents();
		setVisible(true);
	}
	
	@Override
	public void inicialitzacions() {
		Container c=this.getContentPane();
		((JComponent) c).setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.CYAN),"Dades del Punt d'Interes",TitledBorder.CENTER,TitledBorder.DEFAULT_JUSTIFICATION,Fonts.fontTitol(),Color.CYAN));
		
		getContentPane().setLayout(layout= new GridBagLayout());
		controlador= new Controlador();
		finestraPrincipal=this;
		
		Util.treureBarraTitolInteralFrame(this);
		
	}

	@Override
	public void crearComponents() {
			//definicio jcombo
			
			
			
			cmbPunt= new JComboBox<String>(omplirCombo());
			cmbPunt.setSelectedIndex(0);
			cmbPunt.setActionCommand("combo");
			cmbPunt.addActionListener(controlador);
			
			
			//definicio taula . Classe anonima
			model= new DefaultTableModel() {
				private static final long serialVersionUID = 1L;
				@Override
				public boolean isCellEditable( int row, int colum) {
					return false;
				}
			};
			
			//Afeguir columnes a la taula
			model.addColumn("Nom");
			model.addColumn("Descripcio");
			model.addColumn("Gastronòmic");
			model.addColumn("Cultural");
			model.addColumn("Oci");
			model.addColumn("Esport");
			model.addColumn("Platja");
			model.addColumn("Muntanya");
			model.addColumn("Rural");
			model.addColumn("Esquí");
			model.addColumn("Natació");
			model.addColumn("Escalada");
			model.addColumn("Compres");
			model.addColumn("Equitació");
			model.addColumn("Senderisme");
			model.addColumn("Imatge");
			
			taula= new JTable(model);
			taula.setBorder(BorderFactory.createLineBorder(Color.GRAY));
			
			slpTaula= new JScrollPane(taula);
			slpTaula.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			slpTaula.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			
			
			
			
			//label
			lbltaula= new JLabel("Punt d'Interes: ");
			
			
			btnAcceptar= new JButton("Aceptar");
			btnAcceptar.addActionListener(controlador);
			btnAcceptar.setActionCommand("aceptar");
			
			panell= new JPanel(new GridLayout());
			panell.add(btnAcceptar);
			
			
		}
		
		private Vector<String> omplirCombo(){
			
			Map<String,Punt_Interes> puntInt= ControladorPre.getAllPuntI2();
			Vector<String> dadesCombo= new Vector<String>();
			dadesCombo.add(TRIA_EL_PUNT_INT);
			for(Entry<String,Punt_Interes> key: puntInt.entrySet()) {
				dadesCombo.add(key.getValue().getNom());
			}
			return dadesCombo;
		}
		//---------------------------


	@Override
	public void afegirComponenets() {
		
		add(cmbPunt);
		add(slpTaula);
		add(lbltaula);
		add(panell);
		
	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc= new GridBagConstraints();
		
	
		
		//RESTRINCCIONS DEL COMBO
		gbc.gridx=0;//columna
		gbc.gridy=1;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(cmbPunt,gbc);
		
		
		//RESTRINCCIONS DE LA ETIQUETA TAULA
		gbc.gridx=0;//columna
		gbc.gridy=2;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes	
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(lbltaula,gbc);
		
		//RESTRINCCIONS DE LA TAULA
		gbc.gridx=0;//columna
		gbc.gridy=3;//fila
		gbc.gridheight=2;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 1;//creixement columna
		gbc.weighty= 1;//creixement fila
		//gbc.anchor=GridBagConstraints.NONE;
		gbc.fill=GridBagConstraints.BOTH;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(slpTaula,gbc);
		
		//RESTRINCCIONS DEL panell
		gbc.gridx=0;//columna
		gbc.gridy=5;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.EAST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(panell,gbc);
		
	}
	
	public class Controlador implements ActionListener{
		
		
		
		@Override
		public void actionPerformed(ActionEvent e) {
		
			Object obj=e.getSource();
			
			if(obj instanceof JButton) {
				JButton boto= (JButton) obj;
				if(boto.getActionCommand().equals("aceptar")) {
					ControladorPre.canviPantalla(new FormulariDefault());
				}
			}else if(obj instanceof JComboBox<?>) {
				if(cmbPunt.getActionCommand().equals("combo")) {
					omplirTaulaPuntInteresSeleccionat();
				}
			}
			
		}

		
		
		private void omplirTaulaPuntInteresSeleccionat() {
			model.setRowCount(0);//borrar dades de la taula
			if(cmbPunt.getSelectedIndex()!=0) {
				String nom= String.valueOf(cmbPunt.getSelectedItem());
				System.out.println("Nom del Punt d'Interes selecionat al combo: "+ nom);
				afegirPuntInteresATaula(nom);	
				
			}
		}
		
		private void afegirPuntInteresATaula(String nom) {

			
			Punt_Interes puntInt=ControladorPre.getPuntInt(nom);
			
			Object[] objFila= new Object[16];
		
			objFila[0]= puntInt.getNom();
			objFila[1]= puntInt.getDescripcio();
			objFila[2]= puntInt.getGastronomic();
			objFila[3]= puntInt.getCultural();
			objFila[4]= puntInt.getOci();
			objFila[5]= puntInt.getEsport();
			objFila[6]= puntInt.getPlatja();
			objFila[7]= puntInt.getMuntanya();
			objFila[8]= puntInt.getRural();
			objFila[9]= puntInt.getEsqui();
			objFila[10]= puntInt.getNatacio();
			objFila[11]= puntInt.getEscalada();
			objFila[12]= puntInt.getCompres();
			objFila[13]= puntInt.getEquitacio();
			objFila[14]= puntInt.getSenderisme();
			objFila[15]= puntInt.getImatge();
			model.addRow(objFila);
			
			}
		}
		
}



