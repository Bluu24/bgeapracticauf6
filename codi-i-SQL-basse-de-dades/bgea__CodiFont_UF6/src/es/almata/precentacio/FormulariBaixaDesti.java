package es.almata.precentacio;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import es.almata.precentacio.ControladorPre;
import es.almata.precentacio.FormulariDefault;

import es.almata.domini.Destinacio;
import es.almata.exepccions.GestorExepcions;
import es.almata.utils.Fonts;
import es.almata.utils.Util;

public class FormulariBaixaDesti extends JInternalFrame implements Formulari{

	private static final Object BOTO_BAIXA = "baixa";
	private static final String TRIA_EL_DESTI = "TRIA DESTI ";
	
	private static final long serialVersionUID = 1L;


	
	private GridBagLayout layout;
	private Controlador controlador;
	private FormulariBaixaDesti fb;
	
	//---------------------------
	private JComboBox<String> cmbDesti;
	private DefaultTableModel model;
	private JTable taula;
	private JLabel lbltaula;
	private JButton btnCancelar;
	private JScrollPane slpTaula;
	private JButton btnBaixa;
	private JPanel panell;
	
	//---------------------------

	public FormulariBaixaDesti() {
		inicialitzacions();
		crearComponents();
		afegirComponenets();
		posicionarComponents();
		setVisible(true);
	}

	//---------------------------

	@Override
	public void inicialitzacions() {
		Container c=this.getContentPane();
		((JComponent) c).setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY),"Baixa de Destinació",TitledBorder.CENTER,TitledBorder.DEFAULT_JUSTIFICATION,Fonts.fontTitol(),Color.GRAY));
		
		getContentPane().setLayout(layout= new GridBagLayout());
		controlador= new Controlador();
		fb=this;
		
		Util.treureBarraTitolInteralFrame(this);
		
	}

	//---------------------------

	@Override
	public void crearComponents() {
		//definicio jcombo
		
		cmbDesti= new JComboBox<String>(omplirCombo());
		cmbDesti.setSelectedIndex(0);
		cmbDesti.setActionCommand("combo");
		cmbDesti.addActionListener(controlador);

		
		//definicio taula . Classe anonima
		model= new DefaultTableModel() {
			private static final long serialVersionUID = 1L;
			@Override
			public boolean isCellEditable( int row, int colum) {
				return false;
			}
		};
		
		//Afeguir columnes a la taula
		model.addColumn("Provincia");
		model.addColumn("Comarca");
		model.addColumn("Ciutat / Poblacio");
		model.addColumn("Codi Postal");
		model.addColumn("Te Platja ?");
		model.addColumn("Te Muntnya ?");
		
		taula= new JTable(model);
		taula.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		
		slpTaula= new JScrollPane(taula);
		slpTaula.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		slpTaula.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		
		
		
		//label
		lbltaula= new JLabel("Destinació: ");
		
		btnCancelar= new JButton("Cancelar");
		btnCancelar.addActionListener(controlador);
		btnCancelar.setActionCommand("cancelar");
		
		btnBaixa= new JButton("Baixa");
		btnBaixa.addActionListener(controlador);
		btnBaixa.setActionCommand("baixa");
		
		panell= new JPanel(new GridLayout());
		panell.add(btnBaixa);
		panell.add(btnCancelar);
		
		
	}
	
	private Vector<String> omplirCombo(){
		Map<String,Destinacio> desti= ControladorPre.getAllDestins();
		Vector<String> dadesCombo= new Vector<String>();
		dadesCombo.add(TRIA_EL_DESTI);
		for(Entry<String,Destinacio> key: desti.entrySet()) {
			dadesCombo.add(key.getValue().getCiutat_poblacio());
		}
		return dadesCombo;
	}
	//---------------------------


	@Override
	public void afegirComponenets() {
		add(cmbDesti);
		add(slpTaula);
		add(lbltaula);
		add(panell);
		
	}
	//---------------------------


	@Override
	public void posicionarComponents() {
GridBagConstraints gbc= new GridBagConstraints();
		
		//RESTRINCCIONS DEL COMBO
		gbc.gridx=0;//columna
		gbc.gridy=0;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(cmbDesti,gbc);
		
		
		//RESTRINCCIONS DE LA ETIQUETA TAULA
		gbc.gridx=0;//columna
		gbc.gridy=1;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes	
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(lbltaula,gbc);
		
		//RESTRINCCIONS DE LA TAULA
		gbc.gridx=0;//columna
		gbc.gridy=2;//fila
		gbc.gridheight=2;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 1;//creixement columna
		gbc.weighty= 1;//creixement fila
		//gbc.anchor=GridBagConstraints.NONE;
		gbc.fill=GridBagConstraints.BOTH;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(slpTaula,gbc);
		
		//RESTRINCCIONS DEL panell
		gbc.gridx=0;//columna
		gbc.gridy=4;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.EAST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(panell,gbc);
		
		 
		
	}

	//---------------------------

	
	public class Controlador implements ActionListener{
	
		
		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj=e.getSource();
			
			if(obj instanceof JButton) {
				JButton boto= (JButton) obj;
				if(boto.getActionCommand().equals(BOTO_BAIXA)) {
					try {
						Object[]objTExtBotons= {"Acceptar", "Cancel·lar"};
						
						int n= JOptionPane.showOptionDialog(fb, "Segur que vols fer la baixa?", "IMPORTANT", JOptionPane.OK_CANCEL_OPTION , JOptionPane.WARNING_MESSAGE, null, objTExtBotons, objTExtBotons[1]);
						
						
						
						if(!String.valueOf( cmbDesti.getSelectedItem()).equals("TRIA DESTI ")&& n==0) {
						ControladorPre.removeDesti(String.valueOf( cmbDesti.getSelectedItem()));
						}
					}catch(GestorExepcions ge) {

						Object[]objTExtBotons= {"Acceptar"};
						
						JOptionPane.showOptionDialog(fb, "Error SQL: "+ge.getObjecteExepcio().getMessage()+"\nClasse: "+ge.getNomClasse(), "FINESTRA D'INFORMACIÓ", JOptionPane.OK_OPTION , JOptionPane.WARNING_MESSAGE, null, objTExtBotons, objTExtBotons[0]);
						
						Object[]objTExtBotons2= {"Acceptar"};
						
						JOptionPane.showOptionDialog(fb, "Error Coneccio: "+ge.getObjecteExepcio().getMessage()+"\nClasse: "+ge.getNomClasse(), "FINESTRA D'INFORMACIÓ", JOptionPane.OK_OPTION , JOptionPane.WARNING_MESSAGE, null, objTExtBotons2, objTExtBotons2[0]);
					
					}
				}
				
				
				
				
				ControladorPre.canviPantalla(new FormulariDefault());
			} else if(obj instanceof JComboBox<?>) {
				if(cmbDesti.getActionCommand().equals("combo")) {
					omplirTaulaDestiSeleccionat();
				}
			}
		}	
	
		private void omplirTaulaDestiSeleccionat() {
			model.setRowCount(0);
			if(cmbDesti.getSelectedIndex()!=0) {
				String nomCiutat= String.valueOf( cmbDesti.getSelectedItem());
				System.out.println("Nom Desti selecionat combo: "+ nomCiutat);
				afegirDestiATaula(nomCiutat);	
				
			}
		}
		
		
		private void afegirDestiATaula(String nomCiutat) {
			Destinacio desti= ControladorPre.getDesti(nomCiutat);
				
				
				Object[] objFila= new Object[6];
				objFila[0]= desti.getProvincia();
				objFila[1]= desti.getComarca();
				objFila[2]= desti.getCiutat_poblacio();
				objFila[3]= desti.getCp();
				objFila[4]= desti.getPlatja();
				objFila[5]= desti.getMuntanya();
				model.addRow(objFila);
			}
			
			
		}
	
	
	
	
	
	
	
	
	}
//}
