package es.almata.precentacio;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.text.MaskFormatter;

import es.almata.domini.Destinacio;
import es.almata.utils.Encriptar;
import es.almata.utils.Fonts;
import es.almata.utils.Util;

public class FormulariComprovacioEncriptat extends JInternalFrame implements Formulari{

	private static final String PROVINCIA_A_COMPARAR = "Provincia a comparar: ";
	private static final String PROBINCIÁ = "Probinciá: ";
	private static final String BOTO_CANCELAR = "Cancelar";
	private static final String BOTO_ACCEPTAR = "Acceptar";

	
	
	private static final long serialVersionUID = 1L;
	private GridBagLayout layout;
	private Controlador controlador;
	private FormulariComprovacioEncriptat fce;
	
	//---------------------------
	
	private JLabel lblComprovat;
	private JTextField txtComprovar;
	//---------------------------
	private JLabel lblProvincia;
	private JTextField txtProvincia;
	//--------------------------
	
	private JButton btnAcceptar;
	private JButton btnCancelar;
	private JPanel pnlBotons;
	//---------------------------
	
	private JComboBox<String> cmbDesti;
	
	public FormulariComprovacioEncriptat() {
		inicialitzacions();
		crearComponents();
		afegirComponenets();
		posicionarComponents();
		setVisible(true);
	}
	
	@Override
	public void inicialitzacions() {
		Container c=this.getContentPane();
		((JComponent) c).setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GREEN),"Modificacio de Destinació",TitledBorder.CENTER,TitledBorder.DEFAULT_JUSTIFICATION,Fonts.fontTitol(),Color.GREEN));
		
		getContentPane().setLayout(layout= new GridBagLayout());
		controlador= new Controlador();
		fce=this;
		
		Util.treureBarraTitolInteralFrame(this);
		
	}


	@Override
	public void crearComponents() {
		lblComprovat = new JLabel(PROVINCIA_A_COMPARAR);
		txtComprovar = new JTextField();
		txtComprovar.setFont(Fonts.fontTextField());
		
		//---------------------------
		
		btnAcceptar = new JButton(BOTO_ACCEPTAR);
		btnAcceptar.addActionListener(controlador);
		btnAcceptar.setActionCommand(BOTO_ACCEPTAR);
		btnCancelar = new JButton(BOTO_CANCELAR);
		btnCancelar.addActionListener(controlador);
		btnCancelar.setActionCommand(BOTO_CANCELAR);
		pnlBotons= new JPanel();
		pnlBotons.add(btnAcceptar);
		pnlBotons.add(btnCancelar);
		//---------------------------
		
		lblProvincia = new JLabel(PROBINCIÁ);
		txtProvincia = new JTextField();
		txtProvincia.setFont(Fonts.fontTextField());
		
		//---------------------------
		cmbDesti= new JComboBox<String>(omplirCombo());
		cmbDesti.setSelectedIndex(0); //per defecte esta seleccionat la primera opcio -->"tria usuari"
		cmbDesti.setActionCommand("combo");
		cmbDesti.addActionListener(controlador);
		
	}
	
	private Vector<String> omplirCombo(){
		Map<String,Destinacio> desti= ControladorPre.getAllDestins();
		Vector<String> dadesCombo= new Vector<String>();
		dadesCombo.add("TRIA DESTINACIO ");
		for(Entry<String,Destinacio> key: desti.entrySet()) {
			dadesCombo.add(key.getValue().getCiutat_poblacio());
		}
		return dadesCombo;
	}

	@Override
	public void afegirComponenets() {
		
		this.getContentPane().add(lblComprovat);
		this.getContentPane().add(txtComprovar);
		
		this.getContentPane().add(pnlBotons);
		
		this.getContentPane().add(lblProvincia);
		this.getContentPane().add(txtProvincia);
		
		this.getContentPane().add(cmbDesti);
	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc= new GridBagConstraints();
		
		//RESTRINCCIONS DEL COMBO
		gbc.gridx=0;//columna
		gbc.gridy=0;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.insets= new Insets(5, 5, 5, 5);//top, left, bottom, right -> marge entre els components
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya 
		layout.setConstraints(cmbDesti,gbc);
		
		
		
		
		//RESTRINCCIONS DE LA ETIQUETA (Comarca)...
		gbc.gridx=0;//columna
		gbc.gridy=2;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.BOTH;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(lblProvincia,gbc);
		
		//RESTRINCCIONS DEL QUADRO DE TEXT COMARCA
		gbc.gridx=1;//columna
		gbc.gridy=2;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.HORIZONTAL;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(txtProvincia,gbc);
		
		//RESTRINCCIONS DE LA ETIQUETA (Ciutat / Poblacio)...
		gbc.gridx=0;//columna
		gbc.gridy=3;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.BOTH;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(lblComprovat,gbc);
		
		//RESTRINCCIONS DEL QUADRO DE TEXT Ciutat / Poblacio
		gbc.gridx=1;//columna
		gbc.gridy=3;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.HORIZONTAL;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(txtComprovar,gbc);
		
		

		//RESTRINCCIONS DEL PANEL
		gbc.gridx=1;//columna
		gbc.gridy=8;//fila
		gbc.gridheight=1;//alçada en files
		gbc.gridwidth= 1;//amplada en columnes
		gbc.weightx= 0;//creixement columna
		gbc.weighty= 0;//creixement fila
		gbc.anchor=GridBagConstraints.EAST;
		gbc.fill=GridBagConstraints.NONE;//s'adapti al canvi de mesura de la pes tanya
		layout.setConstraints(pnlBotons,gbc);
		
		
	}
	
	public class Controlador implements ActionListener{
		String cami;
		Destinacio desti;
		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj=e.getSource();
			
			if(obj instanceof JButton) {
				JButton boto= (JButton) obj;
				if(boto.getActionCommand().equals(BOTO_ACCEPTAR)) {
					System.out.println("He clicat el botó acceptar!!");	
					
					 String c2 =getStringMessageDigest(txtComprovar.getText(),Encriptar.MD2 );
					 dadesiguals(c2);
					 netejarFormulari();
					
				}else if(boto.getActionCommand().equals(BOTO_CANCELAR)) {
					System.out.println("He clicat el botó cancel·lar!!");
					ControladorPre.canviPantalla(new FormulariDefault());
				}
			}else if(obj instanceof JComboBox<?>) {
				if(cmbDesti.getActionCommand().equals("combo")) {
					omplirFormulariDestiSeleccionat();
					
				}
			}			
		}
		
		

		private void netejarFormulari() {
			txtProvincia.setText("");
			txtComprovar.setText("");
			
		}



		private void omplirFormulariDestiSeleccionat() {
			if(cmbDesti.getSelectedIndex()!=0) {
				String nomCiutat= String.valueOf( cmbDesti.getSelectedItem());
				System.out.println("Nom del Desti selecionat combo: "+ nomCiutat);
				afegirDestiFormulari(nomCiutat);	
			}
			
		}
		
	
		private void afegirDestiFormulari(String nomCiutat) {
			Destinacio desti= ControladorPre.getDesti(nomCiutat);
		
			
			txtProvincia.setText(desti.getProvincia());
		
			
		}
		
		private static String toHexadecimal(byte[] digest){
	        String hash = "";
	        for(byte aux : digest) {
	            int b = aux & 0xff;
	            if (Integer.toHexString(b).length() == 1) hash += "0";
	            hash += Integer.toHexString(b);
	        }
	        return hash;
	    }
		
		 public static String getStringMessageDigest(String message, String algorithm){
		        byte[] digest = null;
		        byte[] buffer = message.getBytes();
		        try {
		            MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
		            messageDigest.reset();
		            messageDigest.update(buffer);
		            digest = messageDigest.digest();
		        } catch (NoSuchAlgorithmException ex) {
		            System.out.println("Error creant el Digest...");
		        }
		        return toHexadecimal(digest);
		    } 
		
		
		 
		private void dadesiguals(String c2) {
		
			Object[]objTExtBotons= {"Acceptar"};
						
					
			if(txtProvincia.getText().equals(c2)) JOptionPane.showOptionDialog(fce, "Les Provincies son iguals", "FINESTRA DE INFO", JOptionPane.OK_OPTION , JOptionPane.WARNING_MESSAGE, null, objTExtBotons, null);
	    	else  JOptionPane.showOptionDialog(fce, "Les Provincies son diferents", "FINESTRA DE INFO", JOptionPane.OK_OPTION , JOptionPane.WARNING_MESSAGE, null, objTExtBotons, null);;
			
			
		}
		


		
		
	}

}
