package es.almata;

import javax.swing.UIManager;


import es.almata.precentacio.ControladorPre;


import es.almata.precentacio.FormulariDefault;


public class Run {

	public static void main(String[] args) {
		
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				try
				{
				  
					UIManager.setLookAndFeel("com.jtattoo.plaf.mint.MintLookAndFeel");
		
				}catch (Exception e)
				{
				   e.printStackTrace();
				}
				
				ControladorPre.crearAplicacio();
				ControladorPre.canviPantalla(new FormulariDefault());
				
			}
		});
		
		
		
	}

}
