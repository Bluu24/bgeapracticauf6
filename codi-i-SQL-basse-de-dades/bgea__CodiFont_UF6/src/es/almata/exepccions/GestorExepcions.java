package es.almata.exepccions;

public class GestorExepcions extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	private String missatge;
	private Throwable objecteExepcio;
	private String nomClasse;
	
	
	
	//-----------------------------------
	
	public GestorExepcions() {
		super();
		
	}

	public GestorExepcions(String message, Throwable cause, String nomClasse) {
		this(message, cause);
		this.nomClasse=nomClasse;
	}
	
	public GestorExepcions(String message, Throwable cause) {
		super(message, cause);
		missatge=message;
		objecteExepcio=cause;
	}

	public GestorExepcions(String message) {
		super(message);
		missatge=message;
	}

	public GestorExepcions(Throwable cause) {
		super(cause);	
		objecteExepcio=cause;
		
	}

	//------------------------------
	public String getMissatge() {
		return missatge;
	}

	public void setMissatge(String missatge) {
		this.missatge = missatge;
	}

	public Throwable getObjecteExepcio() {
		return objecteExepcio;
	}

	public void setObjecteExepcio(Throwable objecteExepcio) {
		this.objecteExepcio = objecteExepcio;
	}

	public String getNomClasse() {
		return nomClasse;
	}

	public void setNomClasse(String nomClasse) {
		this.nomClasse = nomClasse;
	}
	
	
	
	
	

}
