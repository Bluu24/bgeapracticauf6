package es.almata.utils;


	import java.security.MessageDigest;
	import java.security.NoSuchAlgorithmException;

	/**
	 *  Facilita serveis necessaris per l'encriptació de dades.
	 *  S'utilitza en l'encriptaci de la contrasenya d'entrada a l'aplicació
	 *  @author Antoni
	 *  @version 1.0
	 */	
	public class Encriptar {

		    //algoritmes
		    public static String MD2 = "MD2";
		    public static String MD5 = "MD5";
		    public static String SHA1 = "SHA-1";
		    public static String SHA256 = "SHA-256";
		    public static String SHA384 = "SHA-384";
		    public static String SHA512 = "SHA-512";

		    /***
		     * Converteix un vector de bytes en una cadena utilitzant valors hexadecimales
		     * @param digest vector de bytes a convertir
		     * @return String creat a partir de <code>digest</code>
		     */
		    private static String toHexadecimal(byte[] digest){
		        String hash = "";
		        for(byte aux : digest) {
		            int b = aux & 0xff;
		            if (Integer.toHexString(b).length() == 1) hash += "0";
		            hash += Integer.toHexString(b);
		        }
		        return hash;
		    }

		    /***
		     * Encripta un missatge de text mitjançant un algoritme de resum de missatge (hash).
		     * @param message text a encriptar
		     * @param algorithm algoritme d'encriptació, les opcions poden ser: MD2, MD5, SHA-1, SHA-256, SHA-384, SHA-512
		     * @return missatge encriptat
		     */
		    public static String getStringMessageDigest(String message, String algorithm){
		        byte[] digest = null;
		        byte[] buffer = message.getBytes();
		        try {
		            MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
		            messageDigest.reset();
		            messageDigest.update(buffer);
		            digest = messageDigest.digest();
		        } catch (NoSuchAlgorithmException ex) {
		            System.out.println("Error creant el Digest...");
		        }
		        return toHexadecimal(digest);
		    } 
		    
		    
		    public static void main(String[] args){
		    	//encriptar e= new encriptar();
		    	
		    	String c1=getStringMessageDigest("antoni",Encriptar.SHA512 );
		    	String c2=getStringMessageDigest("antoni",Encriptar.SHA512 );
		    	
		    	System.out.println(c1);
		    	System.out.println(c2);
		    	
		    	if(c1.equals(c2)) System.out.println("iguals");
		    	else  System.out.println("diferents");
		            String mensaje = "Missatge secret";
		            System.out.println("Mensaje = " + mensaje);
		            System.out.println("MD2 = " + getStringMessageDigest(mensaje, Encriptar.MD2));
		            System.out.println("MD5 = " + getStringMessageDigest(mensaje, Encriptar.MD5));
		            System.out.println("SHA-1 = " + getStringMessageDigest(mensaje, Encriptar.SHA1));
		            System.out.println("SHA-256 = " + getStringMessageDigest(mensaje, Encriptar.SHA256));
		            System.out.println("SHA-384 = " + getStringMessageDigest(mensaje, Encriptar.SHA384));
		            System.out.println("SHA-512 = " + getStringMessageDigest(mensaje, Encriptar.SHA512));
		            
		            System.out.println("SHA-512 = " + getStringMessageDigest("bgea", Encriptar.SHA512));
		            
		            String usuari="bgea";
		            getStringMessageDigest("bgea", Encriptar.SHA512).equals(getStringMessageDigest(usuari, Encriptar.SHA512));
		        
		    }
		    
		    
	

}
