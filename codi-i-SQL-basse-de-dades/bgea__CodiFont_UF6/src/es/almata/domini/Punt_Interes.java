package es.almata.domini;

import java.util.Objects;

public class Punt_Interes {
	
	private int ID;
	private String nom;
	private String descripcio;
	private String imatge;
	private boolean platja;
	private boolean muntanya;
	private boolean gastronomic;
	private boolean cultural;
	private boolean oci;
	private boolean esport;
	private boolean rural;
	private boolean esqui;
	private boolean natacio;
	private boolean escalada;
	private boolean compres;
	private boolean equitacio;
	private boolean senderisme;
	
	private String fk_Destinacio;
	
	
	//CONSTRUCTORS
	

	public Punt_Interes(int id) {
		super();
		this.ID = id;
	}


	public Punt_Interes() {
		super();
	}
	
	public Punt_Interes(int ID,String nom, String descripcio, String imatge ,boolean platja, boolean muntanya,
			boolean gastronomic, boolean cultural, boolean oci, boolean esport, boolean rural, boolean esqui,
			boolean natacio, boolean escalada, boolean compres, boolean equitacio, boolean senderisme) {
		super();
		this.ID=ID;
		this.nom = nom;
		this.descripcio = descripcio;
		this.imatge = imatge;
		this.platja = platja;
		this.muntanya = muntanya;
		this.gastronomic = gastronomic;
		this.cultural = cultural;
		this.oci = oci;
		this.esport = esport;
		this.rural = rural;
		this.esqui = esqui;
		this.natacio = natacio;
		this.escalada = escalada;
		this.compres = compres;
		this.equitacio = equitacio;
		this.senderisme = senderisme;
	}

	
	
	
	
	//SETTERS AND GETTERS
	
	public int getID() {
		return	ID;
	}

	public void setID(int id) {
		this.ID = id;
	}


	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	
	public boolean getPlatja() {
		return platja;
	}


	public void setPlatja(boolean platja) {
		this.platja = platja;
	}

	public boolean getMuntanya() {
		return muntanya;
	}

	public void setMuntanya(boolean muntanya) {
		this.muntanya = muntanya;
	}

	public boolean getGastronomic() {
		return gastronomic;
	}

	public void setGastronomic(boolean gastronomic) {
		this.gastronomic = gastronomic;
	}

	public boolean getCultural() {
		return cultural;
	}

	public void setCultural(boolean cultural) {
		this.cultural = cultural;
	}

	public boolean getOci() {
		return oci;
	}

	public void setOci(boolean oci) {
		this.oci = oci;
	}

	public boolean getEsport() {
		return esport;
	}

	public void setEsport(boolean esport) {
		this.esport = esport;
	}

	public boolean getRural() {
		return rural;
	}

	public void setRural(boolean rural) {
		this.rural = rural;
	}

	public boolean getEsqui() {
		return esqui;
	}


	public void setEsqui(boolean esqui) {
		this.esqui = esqui;
	}


	public boolean getNatacio() {
		return natacio;
	}


	public void setNatacio(boolean natacio) {
		this.natacio = natacio;
	}


	public boolean getEscalada() {
		return escalada;
	}


	public void setEscalada(boolean escalada) {
		this.escalada = escalada;
	}


	public boolean getCompres() {
		return compres;
	}


	public void setCompres(boolean compres) {
		this.compres = compres;
	}
	

	public boolean getEquitacio() {
		return equitacio;
	}


	public void setEquitacio(boolean equitacio) {
		this.equitacio = equitacio;
	}


	public boolean getSenderisme() {
		return senderisme;
	}


	public void setSenderisme(boolean senderisme) {
		this.senderisme = senderisme;
	}


	public String getfk_Destinacio() {
		return fk_Destinacio;
	}

	public void setfk_Destinacio(String ciutat_poblacio) {
		this.fk_Destinacio = ciutat_poblacio;
	}
	
	public String getImatge() {
		return imatge;
	}
	public void setImatge(String imatge) {
		this.imatge = imatge;
	}

	// TO STRING
	@Override
	public String toString() {
		return "Punt_Intere [ID=" + ID + ",nom=" + nom + ", descripcio=" + descripcio + ", imatge=" + imatge
				+ ", platja=" + platja + ", muntanya=" + muntanya + ", gastronomic=" + gastronomic + ", cultural="
				+ cultural + ", oci=" + oci + ", esport=" + esport + ", rural=" + rural + ", esqui=" + esqui
				+ ", natacio=" + natacio + ", escalada=" + escalada + ", compres=" + compres + ", equitacio="
				+ equitacio + ", senderisme=" + senderisme + ", Destincacio=" + fk_Destinacio + "]";
	}


	

	// HAS CODE AMD EQUALS

	@Override
	public int hashCode() {
		return Objects.hash(ID);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Punt_Interes other = (Punt_Interes) obj;
		return ID == other.ID;
	}
	  	


	

	
	


	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
