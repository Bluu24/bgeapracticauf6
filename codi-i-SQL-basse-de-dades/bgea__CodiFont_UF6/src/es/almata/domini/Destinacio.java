package es.almata.domini;


import java.util.Objects;


public class Destinacio {


	private String provincia;
	private String comarca;
	private String ciutat_poblacio;
	private int cp;
	private boolean platja;
	private boolean muntanya;
	

	
	//CONSTRUCTORS
	
	

	public Destinacio() {
		super();
	}


	
	public Destinacio(String ciutat_poblacio) {
		super();
		this.ciutat_poblacio = ciutat_poblacio;
	}



	public Destinacio(String provincia, String comarca, String ciutat_poblacio, int cp, boolean platja,
			boolean muntanya) {
		super();
		this.provincia = provincia;
		this.comarca = comarca;
		this.ciutat_poblacio = ciutat_poblacio;
		this.cp = cp;
		this.platja = platja;
		this.muntanya = muntanya;
		
	}
	
	

	
	
	//SETTERS AND GETTERS
	


	public String getProvincia() {
		return provincia;
	}


	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}


	public String getComarca() {
		return comarca;
	}


	public void setComarca(String comarca) {
		this.comarca = comarca;
	}


	public String getCiutat_poblacio() {
		return ciutat_poblacio;
	}


	public void setCiutat_poblacio(String ciutat_poblacio) {
		this.ciutat_poblacio = ciutat_poblacio;
	}


	public int getCp() {
		return cp;
	}


	public void setCp(int cp) {
		this.cp = cp;
	}


	public boolean getPlatja() {
		return platja;
	}


	public void setPlatja(boolean platja) {
		this.platja = platja;
	}


	public boolean getMuntanya() {
		return muntanya;
	}


	public void setMuntanya(boolean muntanya) {
		this.muntanya = muntanya;
	}


	// TO STRING
	
	@Override
	public String toString() {
		return "Destinacio [provincia=" + provincia + ", comarca=" + comarca + ", ciutat_poblacio="
				+ ciutat_poblacio + ", cp=" + cp + ", platja=" + platja + ", muntanya=" + muntanya + "]";
	}


	@Override
	public int hashCode() {
		return Objects.hash(ciutat_poblacio);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Destinacio other = (Destinacio) obj;
		return Objects.equals(ciutat_poblacio, other.ciutat_poblacio);
	}
	
	
}
