-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: mysql:3306
-- Temps de generació: 24-05-2023 a les 17:19:06
-- Versió del servidor: 5.7.42
-- Versió de PHP: 8.1.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de dades: `practicaUF6_BG`
--
CREATE DATABASE IF NOT EXISTS `practicaUF6_BG` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `practicaUF6_BG`;
-- --------------------------------------------------------

--
-- Estructura de la taula `Destinacions`
--

CREATE TABLE `Destinacions` (
  `provincia` varchar(255) NOT NULL,
  `comarca` varchar(255) NOT NULL,
  `ciutat_poblacio` varchar(1000) NOT NULL,
  `cp` int(5) NOT NULL,
  `platja` bit(1) NOT NULL,
  `muntanya` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `Destinacions`
--

INSERT INTO `Destinacions` (`provincia`, `comarca`, `ciutat_poblacio`, `cp`, `platja`, `muntanya`) VALUES
('acdea82d8a25a42c192c41010c04f6c0', 'Barcelnones', 'Barcelona', 44444, b'1', b'0'),
('32ec01ec4a6dac72c0ab96fb34c0b5d1', 'La Noguera', 'Gerb', 25614, b'0', b'1');

-- --------------------------------------------------------

--
-- Estructura de la taula `Punts_Interes`
--

CREATE TABLE `Punts_Interes` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `descripcio` varchar(1000) NOT NULL,
  `imatge` varchar(255) NOT NULL,
  `platja` bit(1) NOT NULL,
  `muntanya` bit(1) NOT NULL,
  `gastronomic` bit(1) NOT NULL,
  `cultural` bit(1) NOT NULL,
  `oci` bit(1) NOT NULL,
  `esport` bit(1) NOT NULL,
  `rural` bit(1) NOT NULL,
  `esqui` bit(1) NOT NULL,
  `natacio` bit(1) NOT NULL,
  `escalada` bit(1) NOT NULL,
  `compres` bit(1) NOT NULL,
  `equitacio` bit(1) NOT NULL,
  `senderisme` bit(1) NOT NULL,
  `fk_Destinacio` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índexs per a les taules bolcades
--

--
-- Índexs per a la taula `Destinacions`
--
ALTER TABLE `Destinacions`
  ADD PRIMARY KEY (`ciutat_poblacio`);

--
-- Índexs per a la taula `Punts_Interes`
--
ALTER TABLE `Punts_Interes`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
