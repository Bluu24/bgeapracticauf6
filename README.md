 # ** bgeaPracticaUF6**
 UF6 - POO. Introducció a la persistència en BD.
 <br><br>

## INDEX
- ### [Instruccio Docker](#instrucions-docker)

- ### [Proves](#proves)

- ### [Opcio Personal](#opcio-personal)


<br>

## Instruccions Docker
 ---
**Les instruccions Docker per crear, iniciar, aturar i esborrar els
contenidors de MySQL i PHPMyAdmin i les seves imatges.**

-Un cop instal·lat a la carpeta on hem posat totes les carpetes (mysql, mysql-connectos-j-8.0.33,etc.) relacionades amb els Docker i les conacions hem de tenir un fitxer "docker-compose.yaml". Aquest ens servirà per a crear el primer contenidor i a més es fa que es persisteixen les BBDD en la carpeta.

Per a fer servir el fitxer .yaml hem d'editar i posar la ruta de la carpeta en es i on farà la connexió i la persistència.

![Fitxer "docker-compose.yaml"](/imatges/Selecció_324.png)

-Un cop configurat tenim diferents comandes per al funcionament són les mateixes tant per a Windows que per a Linux.

1-Crear o iniciar els serveis dels contenidors (aquesta sempre sa de fer primer que les altres un cop després de la instal·lació)

```
  docker-compose up –d
```
2-Iniciar els contenidors que ja s’han creat.

```
  docker-compose start
```
3-Aturar els serveis dels contenidors (és recomanable para'l quan ja així ha cavat d'utilitzar el contenidor per a què no gasta recursos de la màquina)

```
  docker-compose stop
```
4-Per a comprovar l'estat de docker i desl contenidors

```
  docker ps -a
```
5-Esborra els contenidors i les seves imatges

```
  docker-compose rm                    
```
 
<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>
<br>

## Proves Formulari
---
**Un joc de proves que demostrin la correctesa del codi.**

* <u> Alta Destinacions </u>

-Totes les dades introduïdes en el formulari d’alta


![Alta desti amb les dades](/imatges/ad.png)

-No introduint el camp clau del destí 

![Error sense el camp clau de desti](/imatges/adn.png)


-No posem el codi postal amb un numero enter

He possat MaskFormatter per a que sol es pugui introdur 5 numeros

```c

	MaskFormatter mascara = null;	
		try {
			mascara= new MaskFormatter("#####");
			mascara.setPlaceholderCharacter('_');
		} catch (ParseException e) {
			e.printStackTrace();
		}		
		txtCodiPostal= new JFormattedTextField(mascara);

```
![Espai del CP](/imatges/Selecció_333.png)

-Comprovació de que a funcionat anant a la Basses de dades i mirat si a guradate el nou reguistre

![Comprovacio alta desti](/imatges/adc.png)

* <u> Alta Punts Interes </u>

-Un cop dins del Formulari d’alta es tot bloquejat fins que no seleccionem un dest

![Alta Punt Inters bloquejat](/imatges/apit.png)  
![Alta punt dinteres obert](/imatges/api.png)

-Per a introduir la imatge sobre un pestanya per a escolli la imatge 

![Tira imatge](/imatges/apii.png)

-Totes les dades introduïdes

![Alta punt int amb dades](/imatges/apif.png)

-No introduïm el cap clau el nom

![Error sense el camp clau de punt d'interes](/imatges/apid.png)

-Comprovació de que a funcionat anant a la Basses de dades i mirat si a guradate el nou reguistre

![Comprovacio alta punt int](/imatges/apic.png)

* <u> Baixa Destinacions </u>

-Primer seleccionem un destí i ens sortira la informació a la taula

![Desti de baixa selecionat](/imatges/bd.png)

-Al clicar el boto de baixa sens obrira una pestanya per assegurar de donar la baixa i si donem 
acceptar ens realitzara la baixa

![Pestanya peraceptar la baixa o no](/imatges/bdf.png)

-Comprovació de que a funcionat anant a la Basses de dades i mirat si a guradate el nou reguistre

![Comprovacio baixa desti](/imatges/bdc.png)

* <u> Baixa Punts Interes </u>

-Primer seleccionem un destí i ens sortira una finestre que ens indica que em de selecionar la columna de la ID per a poder eliminar el Punt D'Interes

![Punt interes selecionat i amb finestre de advertencia](/imatges/bpi.png)

-Al clicar la ID em de clicar el boto de Baixa i ens sortira una pestanya per assegurar que vol donar de baixa

![Pestanya peraceptar la baixa o no](/imatges/bpis.png)

-Comprovació de que a funcionat anant a la Basses de dades i mirat si a guradate el nou reguistre

![Comprovacio baixa punt int](/imatges/bpic.png)

* <u> Modificacio Destinacions </u>

-Primer em de selecciona el destí un cop seleccionat sens omplira el formulari amb les seves 
dades però el nom de la ciutat / població sortira bloquejat ja que es la única opció que no podem 
deixar que modifico per que es el camp clau.

![Pestanya de modifi bloquejada](/imatges/md.png)

![Pestanya de modifi desbloquejada](/imatges/mdt.png)

-En aquest també ens sortira una finestra si no
posem al codi postal un enter

-No posem el codi postal amb un numero enter

He possat MaskFormatter per a que sol es pugui introdur 5 numeros

```c

	MaskFormatter mascara = null;	
		try {
			mascara= new MaskFormatter("#####");
			mascara.setPlaceholderCharacter('_');
		} catch (ParseException e) {
			e.printStackTrace();
		}		
		txtCodiPostal= new JFormattedTextField(mascara);

```
![Espai del CP](/imatges/Selecció_333.png)

-Un cop canviada la Info em de presionar el boto Modifica 

![Informacio canviada](/imatges/mddc.png)

-Comprovació de que a funcionat anant a la Basses de dades i mirat si a guradate el nou reguistre

![Comprovacio modificacio punt int](/imatges/mddcb.png)

* <u> Modificacio Punts Interes </u>
··----------------------

* <u> Consulta Destinacions </u>

-Combo dels destins

![Opcions del combo](/imatges/cdc.png)


-Tenim que seleccionar un destí en el combo i sop lira la taula amb la seva informació


![Desti seleccionat i informacio](/imatges/ccd.png)


* <u> Consulta Punts Interes </u>

-Combo dels Punts Interes

![Opcions del combo](/imatges/ccd.png)

-Tenim que seleccionar un punt d'interes en el combo i sop lira la taula amb la seva informació

![Punt Int seleccionat i informacio](/imatges/cpi.png)

* <u> Consulta General </u>

-Tenim que seleccionar un destí en el combo i sop lira la taula amb la seva informació
 
![Tots els punts dinteres del desti seleecionat al combo](/imatges/cg.png)

* <u> Errors en MySQL </u>

-No hi ha conexio

![Error de no hi ha conneccio](/imatges/Selecció_327.png)

-Si posem camp clau del destí repetit 

![Error de camp calu de dedsti repetit](/imatges/adnr.png)

-Si posem camp clau del punt interes repetit

![Error el camp clau de pund int esta repetit](/imatges/Selecció_325.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>
<br>

## Opcio personal (
---
**Desenvolupar una opció del programa que vosaltres triareu i que no s’hagi fet a classe**

* <u> Encriptar dedes </u>

-La dades que encripto es la Provincia.
A l'ora de introduir les dades escric el nom de la provincia i me la guarde aencriptade.

-He fet un formulari per a comprovar quina provincia e introduit.

![Dades Encriptades](/imatges/Selecció_329.png)

-En el fromulari em de escollir el desti en un convo i ens retornara la provincia encriptada, tenim que introdur el nom de la provincia per a comprovar si son la mateixa

![Formulari Encriptat](/imatges/Selecció_330.png)

-Provincia correcte

![Formulari Encriptat](/imatges/Selecció_331.png)

-Provincia es diferents

![Formulari Encriptat](/imatges/Selecció_332.png)
 
<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

